#!/bin/sh

version=$1

# Prépare le sommaire du livre et la TOC du livre
cd textes
cp garde/livre.yaml tocbook.md
pandoc -f markdown -t html5 --ascii --template="../templates/select.html5" tocbook.md -o sommaire.html
# pandoc -f markdown -t html5 --ascii --template="../templates/progression.html5" tocbook.md -o progression.html
mv sommaire.html ../html/sommaire.html
# mv progression.html /home/nicolas/gitlab/process-local-lankes/output/progression.html
rm tocbook.md

titrelivre=$(yq r garde/livre.yaml title)

# traitement contenus additionnels
## enlever les titres de niveau 1
grep -v '^#\s' ./additionnels.md > ./additionnel-s.md

## tokenize sur le ##
mkdir add
awk '{if ( $1 == "##" ){FileName = $2;next};{print $0 > "add/"FileName".md"}}' additionnel-s.md
cd add
echo "---" > file1
for var in $(ls *.md); do
# Sépare un md avec frontmatter en un yaml et md
awk '/---/{x=++i;next}{if (x == 1){split(FILENAME, A, "."); print > A[1]".yaml";}if (x == 2){print >FILENAME}}' $var
# ajoute au yaml un entete et une fin "---" (nécessaire pour traitement pandoc)
cat file1 ${var%%.*}.yaml file1 > temp.yaml
type=$(yq r temp.yaml type)

echo "processing ${var%%.*} de type $type"

## Traitement avec template spécifique au type de contenu add
case $type in
  article ) pandoc -f markdown -t markdown --template=../../templates/article.html  $var temp.yaml -o ${var%%.*}.temp.md ;;
  dossier ) pandoc -f markdown -t markdown --template=../../templates/dossier.html  $var temp.yaml -o ${var%%.*}.temp.md ;;
  video ) pandoc -f markdown -t markdown   --template=../../templates/video.html    $var temp.yaml -o ${var%%.*}.temp.md ;;
  image ) pandoc -f markdown -t markdown   --template=../../templates/image.html    $var temp.yaml -o ${var%%.*}.temp.md ;;
  pdf ) pandoc -f markdown -t markdown     --template=../../templates/pdf.html      $var temp.yaml -o ${var%%.*}.temp.md ;;
  pageWeb ) pandoc -f markdown -t html --template=../../templates/pageWeb.html  $var temp.yaml -o ${var%%.*}.temp.md ;;
esac
done
cd ..


# Traitement chapitre
chapitre=("preface" "introduction" "chapitre1" "chapitre2" "chapitre3" "chapitre4" "chapitre5" "chapitre6" "chapitre7" "chapitre8" "chapitreadd" "glossaire" "bibliographie" "index-np" "colophon")
# chapitre=("chapitre3")
for i in ${!chapitre[@]};
do
echo Processing ${chapitre[i]}
cd ${chapitre[i]}
cp -r ../add ./add

cp ${chapitre[i]}.md ${chapitre[i]}.int.md

## premier passage pour remplacer les appels de contenus add.
for contAdd in $(grep 'contenuadd' ${chapitre[i]}.int.md | sed 's/!contenuadd(.\///g' | sed 's/)(\w*)//g'); do
# echo "$contAdd"
sed -i -e '/'"$contAdd"'/r ./add/'"$contAdd"'.temp.md' ${chapitre[i]}.int.md
done
sed -i -e 's/!contenuadd.*//g' ${chapitre[i]}.int.md

## deuxieme passage pour les contenus add de type dossier
for contAdd in $(grep '\[contenuadd\]' ${chapitre[i]}.int.md | sed 's/!\[contenuadd\](.\///g' | sed 's/)(\w*)//g'); do
# echo "$contAdd"
sed -i -e '/'"$contAdd"'/r ./add/'"$contAdd"'.temp.md' ${chapitre[i]}.int.md
done
sed -i -e 's/!\[contenuadd\].*//g' ${chapitre[i]}.int.md


pandoc --standalone --section-divs --ascii --id-prefix=${chapitre[i]}- --filter pandoc-citeproc --filter pandoc-sidenote --template=../../templates/lankes.html5 --table-of-contents ./${chapitre[i]}.int.md ./${chapitre[i]}.yaml  -o ./${chapitre[i]}.html > ../../html/bash.log

rm ${chapitre[i]}.int.md

## Ajout de la navigation

current=$((i + 1))
prec=${chapitre[i-1]}
if [ $((i + 1)) -eq ${#chapitre[@]} ]
then
  next=${chapitre[i-i]}
else
  next=${chapitre[i+1]}
fi

currentTitle=$(yq r ../${chapitre[i]}/${chapitre[i]}.yaml title)
precTitle=$(yq r ../$prec/$prec.yaml title)
nextTitle=$(yq r ../$next/$next.yaml title)


sed -i -e "s/TITRELIVRE/$titrelivre/g" ${chapitre[i]}.html
sed -i -e '/SELECT/r ../../html/sommaire.html' ${chapitre[i]}.html # rajoute le sommaire à l'html
sed -i -e "s/<li><a href=\"${chapitre[i]}.html\">/<li class=\"selected\"><a href=\"${chapitre[i]}.html\">/g" ${chapitre[i]}.html
sed -i -e "s/currentChapitre/${chapitre[i]}/g" ${chapitre[i]}.html
sed -i -e "s/CURRENTTITLE/$currentTitle/g" ${chapitre[i]}.html

sed -i -e "s/<!--version-->/$version/g" ${chapitre[i]}.html

#### nav.prec-next>prec
sed -i -e "s/<a rel=\"prev\" href=\"chapitreprec.html\" title=\"\">/<a rel=\"prev\" href=\"$prec.html\" title=\"$precTitle\">/g" ${chapitre[i]}.html
#### footer>prec
sed -i -e "s/<a href=\"chapitreprec.html\">/<a href=\"$prec.html\">/g" ${chapitre[i]}.html
#### nav.prec-next>next
sed -i -e "s/<a rel=\"next\" href=\"chapitrenext.html\" title=\"\">/<a rel=\"next\" href=\"$next.html\" title=\"$nextTitle\">/g" ${chapitre[i]}.html
#### footer>next
sed -i -e "s/<a href=\"chapitrenext.html\">/<a href=\"$next.html\">/g" ${chapitre[i]}.html
sed -i -e "s/PRECTITLE/$precTitle/g" ${chapitre[i]}.html
sed -i -e "s/NEXTITLE/$nextTitle/g" ${chapitre[i]}.html
sed -i -e "s/CURRENTCHAP/$current/g" ${chapitre[i]}.html
sed -i -e "s/MAXCHAP/${#chapitre[@]}/g" ${chapitre[i]}.html
sed -i -e "s/SELECT//g" ${chapitre[i]}.html

echo "---> Ajout des ids et details"

java  -cp ../../vendor/saxon9he.jar:../../vendor/tagsoup-1.2.1.jar net.sf.saxon.Transform -x:org.ccil.cowan.tagsoup.Parser --suppressXsltNamespaceCheck:on -s:${chapitre[i]}.html -xsl:../../source/addID.xsl -o:${chapitre[i]}.html
mv ${chapitre[i]}.html ../../html/${chapitre[i]}.html
 rm -rf add
cd ..
done
# fin traitement chapitre
rm -rf add
rm additionnel-s.md
cd ..
cp -r ./static ./html/
cp -r ./textes/media ./html/
rm -rf ./html/v$version
cp -r ./html ./v$version
mv ./v$version ./html/

# création de la base de données "lankes"
./vendor/basex/bin/basex ./source/makeDB.bxs

# création de l'index exploitant la db "lankes"
./vendor/basex/bin/basex -o ./index-np.temp.html ./source/makeIndex.xq
# insère l'index dans index-np.html
sed -i -e '/%INDEX%/r ./index-np.temp.html' ./html/index-np.html
sed -i -e 's/%INDEX%//g' ./html/index-np.html

rm ./index-np.temp.html

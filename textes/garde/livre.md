

<!-- ces éléments viendront après la fiche du livre, construite à partir du yaml -->

# Autour du livre

<!-- video de présentation par l'auteur -->

## Kit press
<!-- Communiqué de presse -->

## Recensions



<!-- structure du templateGarde -->

<body>
  <head>lien à la collection</head>
  <aside>
    <nav>tocbook</nav>
    <div id=footerBook></div>
  </aside>
  <article>
    - titre/soustitre
    - couverture
    - 4emecouv
    - infosgénérales par formats:
      - date de parution
      - nbpages
      - version disponibles
    - Présentation auteurs
      - auteur, présentation
    <h2>Autour du livre</h2>
    $body$  <!-- on met ici tout ce qui est dans le livre.md -->
  </article>
</body>
...

<!-- à ajouter à la fin du template  -->
  <script>
    $("#footerBook").load("../footerBook.html");
  </script>

footerBook.html
<div id="footerBook">
  - date parution, version et date version
  - licence
  - reference complete de citation
  - bouton pour copier la citation ou récupérer le bibtex
  - lien à la collection & ateliers
</div>


La bibliothèque de l’[[Université de Syracuse](https://www.syracuse.edu/about/){link-archive="https://web.archive.org/web/20190905182536/https://www.syracuse.edu/about/"}]{.organisme idsp="Université de Syracuse" idwiki="https://www.wikidata.org/wiki/Q617433"} était pleine. Il ne restait plus d’espace sur les étagères. C’est un problème fréquent dans toutes les bibliothèques et les solutions vont de retirer des livres (élagage) à construire de nouveaux bâtiments. [Syracuse]{.lieu idsp="Syracuse (New York, États-Unis)" idwiki="https://www.wikidata.org/wiki/Q128069"} a essayé la première solution, puis la deuxième, mais a finalement opté pour l’entreposage hors site. Les bibliothécaires allaient chercher les items les moins utilisés (ceux n’ayant pas été empruntés depuis au moins 10 ans) et les expédiaient dans un entrepôt situé à cinq heures de distance. Si un item de l’entrepôt était demandé, on le renvoyait à [Syracuse]{.lieu idsp="Syracuse (New York, États-Unis)" idwiki="https://www.wikidata.org/wiki/Q128069"}, ou alors on le numérisait et l’envoyait directement par courriel au professeur·e ou à l’étudiant·e qui en aurait fait la demande.

Vous pourriez croire qu’il n’y aurait pas tant de livres inutilisés sur une période de plus d’une décennie, mais vous auriez tort. En fait, dans n’importe quelle bibliothèque, il faut s'en remettre à la règle du 80/20. Dans une collection, 80 % des utilisations se feront sur 20 % des items de cette collection. Autrement dit, on pourrait jeter 80 % des livres et continuer à satisfaire 80 % des demandes de la communauté. Alors, pourquoi garder le reste&nbsp;? Eh bien parce qu’on ne sait jamais si l’un de ces 20 % d’utilisateurs et d'utilisatrices tireront profit de la lecture de l’un de ces 80 % de livres peu utilisés pour guérir le cancer — et il n’y a pas de façons de le savoir à l’avance.

Mais la bibliothèque de l’[Université de Syracuse]{.organisme idsp="Université de Syracuse" idwiki="https://www.wikidata.org/wiki/Q617433"} ne jetait pas ses livres peu utilisés. Elle ne faisait que les déplacer. Cela semblait logique. Cependant, les départements de sciences humaines du campus faillirent en venir à la révolte. Qu’il s’agisse de professeur·e·s en théologie, de diplômé·e·s en histoire ou d’étudiant·e·s en littérature anglaise, tous prirent le mors aux dents. Des rencontres facultaires ont été perturbées, des manifestations ont eu lieu en bibliothèque, des éditoriaux acérés ont été publiés. «&nbsp;Pourquoi ne pas déplacer les livres dans un endroit plus rapproché&nbsp;?&nbsp;» «&nbsp;Cette collection était déjà de piètre qualité et maintenant vous voulez la rendre encore pire qu’elle ne l’est&nbsp;?&nbsp;»

Bien que les bibliothécaires s’attendaient à une part de résistance face à leur projet d’entreposage hors site, le degré d’opposition les prit par surprise. Durant des années, les bibliothécaires avaient fait croître la fréquentation de la bibliothèque. Grâce à l’introduction d’espaces d’apprentissage, de nouveaux espaces de rencontre, d’un café, de prises électriques et de nouveaux services, la bibliothèque était fréquentée plus que jamais. La bibliothèque était pleine, non seulement en termes de livres, mais en termes de membres. Le problème était que les chercheur·e·s en sciences humaines ne voyaient ni le café ni les étudiant·e·s de premier cycle branché·e·s aux prises disponibles comme s’adonnant à une utilisation appropriée des lieux. Chaque table était de l’espace utilisable pour davantage de rayonnages et de livres. Voilà, dirent les chercheur·e·s, la fonction première de la bibliothèque&nbsp;: contenir des livres et documents imprimés, et non pas des espaces de rencontre et un café.

L’idée suivant laquelle les bibliothèques ne sont qu’un entrepôt de livres n’est pas limitée aux professeur·e·s en sciences humaines.
Quelques années plus tôt à [Syracuse]{.lieu idsp="Syracuse (New York, États-Unis)" idwiki="https://www.wikidata.org/wiki/Q128069"} (apparemment un endroit propice pour les controverses autour des bibliothèques), le comté avait lancé un programme de recyclage de livres. Une fois par année, les habitant·e·s pouvaient emballer leurs vieux livres et les déposer pour être recyclés en pâte à papier. Il y eut immédiatement des plaintes de citoyen·ne·s concerné·e·s, appelant la bibliothèque à intervenir. «&nbsp;Ne recyclez pas les livres, donnez-les à la bibliothèque !&nbsp;» La bibliothèque refusa, non parce qu’elle était pleine, mais parce qu’elle était débordée.

La bibliothèque n’avait pas le personnel suffisant pour trier des milliers de livres, en tâchant d’identifier ceux qui méritaient d’être conservés — du moins c’est ce qu’ils ont dit au début. Quand des membres de la communauté s’organisèrent avec les scouts pour entreprendre le tri à effectuer, la véritable raison éclata au grand jour. Il s’avéra que les bibliothécaires s’étaient déjà rendu·e·s au point de collecte des livres à recycler, et avaient trouvé de vieux livres décrépits et sans valeur. Ils et elles découvrirent aussi que les habitant·e·s avaient profité de l’opportunité pour recycler des items tels que de numéros du magazine pornographique *Hustler*. Les bibliothécaires ne tenaient pas à trier tout cela côte à côte avec les scouts.

Les bibliothèques scolaires reçoivent régulièrement des dons d’exemplaires du *National Geographic* parce qu’ils «&nbsp;doivent&nbsp;» avoir de la valeur. Or les bibliothèques n’ont pas de place pour conserver ces exemplaires imprimés, sans parler du fait que tous les numéros de cette publication sont disponibles en ligne[^NG].

À [Glendale]{.lieu idsp="Glendale (Ohio, États-Unis)" idwiki="https://www.wikidata.org/wiki/Q2667009"}, un village cossu de la banlieue de [Cincinnati]{.lieu idsp="Cincinnati (Ohio, États-Unis)" idwiki="https://www.wikidata.org/wiki/Q43196"}, dans l’[Ohio]{.lieu idsp="Ohio (États-Unis)" idwiki="https://www.wikidata.org/wiki/Q1397"}, les citoyen·ne·s démarrèrent leur propre bibliothèque avec des livres donnés. Ils ont garni les rayonnages et procédé à l’ouverture des lieux.
Après une semaine d’activité, la fréquentation est tombée. Il semble que les gens ne cherchaient pas à lire les livres qu’ils avaient donnés et étaient prêts à se déplacer en voiture pour fréquenter les trois bibliothèques publiques situées dans un rayon de 8 kilomètres.

Ces histoires convergent toutes vers l’un des plus grands mythes à propos des bibliothèques modernes — qu’elles sont juste une question de livres. Si c’est aussi ce que vous pensez, je vous pardonne. Après tout, les bibliothèques ont connu un grand succès dans l’industrie du livre et, au surplus, de nombreuses bibliothèques ont construit cette image de marque livre=bibliothèque dans leurs communautés au moins depuis plus d’un siècle.

À première vue, même les plus célèbres des standards de la bibliothéconomie crient à tue-tête que tout est à propos des livres. En 1931, [S. R. Ranganathan]{.personnalite idsp="S. R. Ranganathan" idbnf="https://catalogue.bnf.fr/ark:/12148/cb121792839"} proposa ses cinq lois de la bibliothéconomie. Ces lois font partie des pierres angulaires de la pensée bibliothéconomique&nbsp;:

1. Les livres sont faits pour être utilisés.
2. À chaque lecteur son livre.
3. À chaque livre son lecteur.
4. Épargnons le temps du lecteur.
5. Une bibliothèque est un organisme en développement.

!contenuadd(./dossierRanganathan)(dossier)

Sans l’ombre d’un doute, l’idée que les bibliothèques dépendent des livres est ancrée profondément dans l’ADN de la bibliothéconomie.

Cela dit, regardons ces lois de plus près. À quel point les livres sont-ils au cœur de ces lois&nbsp;? Si [Ranganathan]{.personnalite idsp="S. R. Ranganathan" idbnf="https://catalogue.bnf.fr/ark:/12148/cb121792839"} avait vécu il y a 2000 ans, aurait-il dit «&nbsp;les parchemins sont faits pour être utilisés&nbsp;»&nbsp;? Si on remplace le mot «&nbsp;livres&nbsp;» par «&nbsp;livres numériques&nbsp;» ou «&nbsp;pages Web&nbsp;», est-ce que ces idées demeurent vraies&nbsp;? Je crois que oui. Ces lois établissent que la communauté est au centre de la bibliothèque. Le travail de la bibliothèque est de combler les besoins des membres de la communauté, pas simplement d’entreposer des documents.

Les bibliothèques, qu’elles soient bonnes ou mauvaises, ont existé depuis des millénaires. Durant tout ce temps, elles ont été des entrepôts de documents, certes, mais tout autant des lieux consacrés à la recherche, à l’archivage de la documentation de l’État et des incubateurs de développement économique. En fait, l’idée selon laquelle une bibliothèque est un édifice plein à craquer de livres et de documents est un point de vue qui date d’à peine 80 ans.

Jetons un coup d’oeil à la [[Free Library de Philadelphie](https://libwww.freelibrary.org/about/){link-archive="https://web.archive.org/web/20190903171303/https://libwww.freelibrary.org/about/"}]{.organisme idsp="Free Library de Philadelphie"} telle qu’elle est aujourd’hui&nbsp;:


![Salle de musique de l’actuelle Free Library de Philadelphie.](media/Fig1SalleMusiqueFreeLibPhil.jpg)

Des livres sur des étagères autour de colonnes&nbsp;: voilà une bibliothèque.
Maintenant, jetons un coup d’œil à la même section de cette bibliothèque dans les années 1920&nbsp;:


![Salle de musique de la Free Library de Philadelphie vers 1927. [Free Library of Philadelphia : Digital Collections](https://libwww.freelibrary.org/digital/index.cfm){.source}](media/Fig2SalleMusiqueFreeLibPhil1927.jpg)

Oui, il s’agit de la même salle. Des tables de travail, un éclairage naturel. Un endroit conçu d’abord pour les personnes et pour l’apprentissage, et non pour les livres. Il y avait évidemment, des livres dans cette bibliothèque&nbsp;; ils étaient sortis sur demande des magasins fermés. Les espaces ouverts étaient pour les personnes, les espaces fermés pour les livres.

Quand a-t-on commencé à penser aux bibliothèques comme étant des paradis du livre&nbsp;? Les bibliothèques ont toujours hébergé des collections de documents, bien que ce concept de dépôt soit lui-même relativement moderne. Il s’est mis en place lorsque les bibliothèques ont tenté de développer des collections exhaustives au moment où l’on assistait à une diminution marquée des prix du papier et de l’impression. Ce n’est qu’au XX^e^ siècle que la production de masse de livres commença à remplir les bibliothèques ainsi que les salons et les écoles.


![Croissance des ouvrages publiés mondialement[^57].](media/Fig3CroissanceOuvragesPublies.jpg)

Cette bibliophilie changea la façon dont nous voyons non seulement les bibliothèques d’aujourd’hui, mais également celles du passé.

[Considérons]{#alexandrieorigine} à nouveau la [[bibliothèque d’Alexandrie](https://www.bibalex.org/fr/Page/About){link-archive="https://web.archive.org/web/20190905182714/https://www.bibalex.org/fr/Page/About"}]{.organisme idsp="Bibliothèque d'Alexandrie" idwiki="https://www.wikidata.org/wiki/Q501851"}, évoquée [au premier chapitre](chapitre1.html#chapitre1-BibliothequeAlexandrie). La [bibliothèque d’origine]{.organisme idsp="Bibliothèque d'Alexandrie" idwiki="https://www.wikidata.org/wiki/Q435"} était une merveille du monde antique. Aujourd’hui, ceux et celles qui la connaissent la perçoivent comme une énorme collection de documents du monde antique. Ce que, d’ailleurs, elle était. Mon histoire favorite est celle des bateaux qui entraient dans le port d’[Alexandrie]{.lieu idsp="Alexandrie (Égypte)" idwiki="https://www.wikidata.org/wiki/Q87"}, qui était jadis l’un des ports les plus achalandés de son temps. Les soldats abordaient les bateaux et confisquaient tous les documents trouvés à bord (incluant ceux qui étaient utilisés comme lest). Les documents étaient alors acheminés à la bibliothèque, puis copiés, et ces copies étaient ensuite remises aux propriétaires des originaux.

Mais si vous pensez à la bibliothèque antique comme étant un gigantesque entrepôt de documents, à l’instar de la photographie actuelle de la [Free Library de Philadelphie]{.organisme idsp="Free Library de Philadelphie"}, vous vous trompez. En fait, la [bibliothèque d’Alexandrie]{.organisme idsp="Bibliothèque d'Alexandrie" idwiki="https://www.wikidata.org/wiki/Q435"} était semblable aux universités d’aujourd’hui.
Il y avait de nombreux édifices sur le campus. L’un des premiers était un temple dédié aux Muses appelé le Musae, d’où nous vient le mot «&nbsp;musée&nbsp;». Le bâtiment principal de la bibliothèque était autant un dortoir qu’un entrepôt. Des savants de partout s’y rassemblaient pour discuter et créer. En fait, il s’agissait là d’un des premiers *think tanks* et l’un des premiers centres d’innovation de l’histoire. Les bibliothécaires étaient les plus proches conseillers des dirigeants de la cité-État, et ce non pas parce qu’ils avaient accès à des documents, mais parce qu’ils étaient en lien direct avec des penseurs.

!contenuadd(./videoBiblioAlexandrie)(video)

Quand la [bibliothèque d’Alexandrie]{.organisme idsp="Bibliothèque d'Alexandrie" idwiki="https://www.wikidata.org/wiki/Q435"} fut détruite, une grande partie de la collection fut en fin de compte relocalisée dans l’[Espagne]{.lieu idsp="Espagne" idwiki="https://www.wikidata.org/wiki/Q29"} mauresque.
Là, loin d’accumuler la poussière, ces documents furent traduits, améliorés, et consultés. Tout ce travail est devenu manifeste lors des premières croisades, à la fin de l’époque médiévale. En effet, lorsque les croisés «&nbsp;libérèrent&nbsp;» la ville de [Tolède]{.lieu idsp="Tolède (Espagne)" idwiki="https://www.wikidata.org/wiki/Q5836"}, ils découvrirent bibliothèque après bibliothèque après bibliothèque. Ce dut être stupéfiant de voir qu’une de ces 80 bibliothèques contenait plus de volumes qu’il n’y en avait dans toute la [France]{.lieu idsp="France" idwiki="https://www.wikidata.org/wiki/Q142"}. Soulignons un fait remarquable&nbsp;: les citoyens ne faisaient pas que préserver les manuscrits, ils les ont consultés pour développer de nouvelles formes d’architecture, de nouveaux aqueducs, de nouveaux modes de gouvernance, allant même jusqu’à recourir à une petite chose connue sous le nom d’algèbre (incluant, soit dit en passant, le concept de zéro). En fait, il se trouve même un historien pour attribuer à ces bibliothèques vivantes du monde musulman le mérite d’avoir rendu possible la Renaissance et la création des universités.

Dans l’[Angleterre]{.lieu idsp="Angleterre" idwiki="https://www.wikidata.org/wiki/Q21"} victorienne, les bibliothèques publiques avaient des salons de jeu. [[Andrew Carnegie](https://www.carnegiecouncil.org/people/andrew-carnegie){link-archive="https://web.archive.org/web/20190905182834/https://www.carnegiecouncil.org/people/andrew-carnegie"}]{.personnalite idsp="Andrew Carnegie" idbnf="https://catalogue.bnf.fr/ark:/12148/cb124591714"} construisit plus de 2509 bibliothèques autour du monde [@noauthor_andrew_2015] pour encourager la participation démocratique et l’amélioration de la société. Ces bibliothèques publiques comprenaient des galeries d’art. Dans la foulée des lois sur le travail des enfants, qui ont permis au concept moderne de l’enfance d’émerger, des collections destinées aux touts petits se sont développées. Les bibliothécaires ont même arraché les rayonnages des bibliothèques mobiles pour en faire des *makerspaces* mobiles. Le [[Frysklab](http://www.frysklab.nl/){link-archive="https://web.archive.org/web/20190905182916/http://www.frysklab.nl/"}]{.organisme idsp="Frysklab"} des [Pays-Bas]{.lieu idsp="Pays-Bas" idwiki="https://www.wikidata.org/wiki/Q55"} est un *makerspace* mobile rempli d’ordinateurs portables, d’imprimantes 3D, et de graveurs laser qui voyage dans les provinces du nord du pays et s’arrête dans les écoles pour enrichir le parcours scolaire des élèves.

Tout cela pour dire que si vous croyez qu’une bibliothèque n’est qu’un amas de livres logé dans un bâtiment (ou pire, si vos bibliothécaires pensent cela), vous devez prétendre à plus, à beaucoup plus. Au lieu d'être des lieux silencieux, les meilleures bibliothèques d’aujourd’hui se transforment en espaces qui admettent le bruit tout en aménageant des espaces de quiétude. Elles passent du domaine des bibliothécaires au domaine des communautés. Qu’est-ce qui guide cette transformation&nbsp;? Qu’est-ce qui façonne «&nbsp;l’organisme en développement&nbsp;» de [Ranganathan]{.personnalite idsp="S. R. Ranganathan" idbnf="https://catalogue.bnf.fr/ark:/12148/cb121792839"}&nbsp;? Cette mission qui court depuis des siècles&nbsp;:

>La mission d’une bibliothèque est d’améliorer la société en facilitant la création de connaissances dans sa communauté.

À dire vrai, cette formulation est la mienne, mais les concepts qu’elle contient trouvent une vérification historique dans la pratique des érudits qui ont mis en place des bibliothèques pour faire avancer les travaux de recherche de leurs collègues. C’est aussi cette mission qui est à l’œuvre dans le travail des bibliothécaires du [Kenya]{.lieu idsp="Kenya" idwiki="https://www.wikidata.org/wiki/Q114"} et de [Ferguson]{.lieu idsp="Ferguson (Missouri, États-Unis)" idwiki="https://www.wikidata.org/wiki/Q954855"} que j’ai évoqué·e·s [en ouverture de ce livre](chapitre1.html#chapitre1-kenya). [Les mauvaises]{#mauvaises} bibliothèques ne font que développer des collections. Les bonnes bibliothèques développent des services (et la collection n’est que l’un d’entre eux). Les meilleures bibliothèques développent des communautés.

Les tablettes de pierre se sont transformées en rouleaux, les rouleaux en manuscrits, les manuscrits en livres, et les livres se transforment en ce moment en applications. Les outils que les bibliothèques utilisent pour accomplir leur mission, quelle que soit cette mission, vont changer. La raison pour laquelle nous utilisons ces outils (nouveaux ou anciens) demeure identique au fil du temps. Les bibliothèques devraient être une affaire de connaissances pas d’outils.

Le reste de ce livre examinera ce que vous êtes en droit d’attendre de la part d’une bibliothèque en se basant sur les différents éléments de l’énoncé de mission ci-dessus (ce que j’entends par améliorer, par connaissance, par faciliter, etc.), mais avant de faire cela il nous faut traiter de deux problèmes&nbsp;: celui de la lecture et celui de l’utilité générale d’avoir une mission.

## J’adore lire… vraiment, j’adore ça

Jetons encore un coup d’œil à cet énoncé de mission&nbsp;: améliorer la société en facilitant la création de connaissances. Mais où sont donc passés la promotion de la lecture et l’amour des livres&nbsp;? Est-ce qu’attendre davantage des bibliothèques signifie abandonner la lecture et la littérature, les romans et la poésie&nbsp;? La lecture n’est pas mentionnée dans cette mission plus générale parce que ce ne sont pas toutes les bibliothèques qui concentrent leurs activités autour de la lecture. Les bibliothèques scolaires et publiques voient la promotion et le développement des habiletés de lecture comme l’un de leurs buts fondamentaux. Quant aux bibliothèques en entreprise et celles en milieu universitaire, elles tiennent pour acquis que les personnes qu’ils et elles desservent possèdent déjà ces habiletés. Qui plus est, bien que la lecture soit une compétence cruciale pour créer de la connaissance, ce n’est pas le seul moyen d’atteindre «&nbsp;l’illumination&nbsp;». Certaines personnes apprennent par le biais de la lecture, d’autres en regardant des vidéos, d’autres encore par le passage à l’action, et la grande majorité d'entre elles combinent tout cela. On devrait attendre de nos bibliothèques qu’elles soutiennent toutes ces modalités d’apprentissage.

Quand les gens me posent des questions sur les bibliothèques, la lecture et la mission que je propose, ils me demandent habituellement&nbsp;: «&nbsp;Est-ce que je ne peux pas tout simplement utiliser la bibliothèque pour lire un bon roman ou emprunter un DVD sans me préoccuper de sauver la planète&nbsp;?
Est-ce que lire pour le plaisir n’a aucune valeur&nbsp;?&nbsp;» À cela, je réponds&nbsp;: oui, et la lecture d’un roman est tout aussi importante pour l’apprentissage et l’acquisition de connaissances que la lecture d’un essai. Les histoires sont pour nous des façons de rêver et de tester nos limites éthiques. Un bon roman peut nous dévoiler des vérités fondamentales d’une manière qui demeurera inaccessible aux traités philosophiques. Sans oublier le fait que les idées et les inspirations pour de grandes actions nous viennent souvent au moment où on s’y attend le moins.

Une grande partie de la littérature en bibliothéconomie porte sur les concepts d’information et d’émancipation, en ignorant souvent ou en prenant tacitement pour acquis que les bibliothèques peuvent aussi soutenir les loisirs et le développement de la lecture. Il ne fait pas de doute que ce livre est axé sur les bibliothèques en tant que lieux d’engagement social et d’apprentissage.
La question n’est pas de savoir si ces dernières devraient ou non soutenir la lecture d’agrément. Il revient en effet à la communauté elle-même de trancher cette question tout comme il lui revient de se demander si elle tient à soutenir les arts ou à aménager des parcs. La vraie question tourne autour des individus et de leur volonté de transformer la lecture récréative en quelque chose de social, ou d’orienté vers un but plus grand.

Imaginons que je lis un livre. Cela me procure du plaisir. Voilà qui peut me suffire. Mais qu’arrive-t-il si une belle œuvre de fiction m’incite à écrire mon propre roman ou à inventer un nouvel appareil ou encore à former un groupe de lecteurs et de lectrices qui adorent ce livre au point de planifier une action collective&nbsp;? Ce n’est pas le rôle de la bibliothèque de déterminer à l’avance les effets que devrait avoir la lecture (ou en encore le fait d’inventer quelque chose ou de réaliser un film), ce qui reviendrait à dire aux gens quoi lire et pour quelle raison. La bibliothèque doit plutôt être une plate-forme qui permet aux membres d’une communauté de transformer leurs intérêts et leurs passions en quelque chose de positif pour leur communauté et/ou pour eux-mêmes.

Plus on fait quelque chose, plus on y devient bon. C’est pourquoi nous devons soutenir toutes les formes de lecture, et ce, dès que c’est approprié (à la bibliothèque, à l’école, sur le terrain de jeux, en vacances, au laboratoire, dans les jeux vidéo). Lorsque vous lirez les mots «&nbsp;connaissance&nbsp;» et «&nbsp;apprentissage&nbsp;» dans la suite de ce livre, ne croyez pas que je les limite aux idées qui se retrouvent dans les manuels et les articles scientifiques. La poésie, les romans et une bonne histoire de science-fiction sont tout aussi importants du point de vue de la création de connaissances. Cela dit, je suis d’avis qu’on devrait aussi attendre de tous les types de bibliothèques qu’elles se tiennent prêtes à encourager les effets de la lecture.

Voyons maintenant comment les bibliothèques abordent ces idées dans leurs énoncés de mission.

## Mission vers nulle part&nbsp;?

Un énoncé de mission est quelque chose d’important. Cela représente une sorte de consensus de ce qu’une organisation juge essentiel. Un énoncé de mission permet de voir comment les bibliothèques établissent des attentes envers elles-mêmes et pour les communautés qu’elles desservent.
Jetons un coup d’œil à la mission de quelques bibliothèques et organisations&nbsp;:

Commençons par l’excellente mission de la [[New York Public Library](https://www.nypl.org/help/about-nypl){link-archive="https://web.archive.org/web/20190905183026/https://www.nypl.org/help/about-nypl"}]{.organisme idsp="New York Public Library" idwiki="https://www.wikidata.org/wiki/Q219555"}&nbsp;:

>La mission de la [New York Public Library]{.organisme idsp="New York Public Library" idwiki="https://www.wikidata.org/wiki/Q219555"} est d’inspirer l’apprentissage tout au long de la vie, de faire progresser les connaissances et de renforcer nos communautés [@noauthor_nypls_2018].

On ne peut guère faire mieux que de faire faire progresser les connaissances et de renforcer nos communautés.

S’agissant de progrès des connaissances, lisons la mission du [[Massachusetts Institute of Technology (MIT)](https://www.mit.edu/about/){link-archive="https://web.archive.org/web/20190905183111/https://www.mit.edu/about/"}]{.organisme idsp="Massachusetts Institute of Technology (MIT)" idwiki="https://www.wikidata.org/wiki/Q49108"}&nbsp;:

>La mission des bibliothèques du [MIT]{.organisme idsp="Massachusetts Institute of Technology (MIT)" idwiki="https://www.wikidata.org/wiki/Q49108"} est de créer et de maintenir un environnement informationnel en évolution qui fasse progresser l’apprentissage, la recherche et l’innovation au [MIT]{.organisme idsp="Massachusetts Institute of Technology (MIT)" idwiki="https://www.wikidata.org/wiki/Q49108"}. Nous nous engageons à l’excellence dans les services, les stratégies et les systèmes qui favorisent la découverte, préservent les connaissances et améliorent la communication savante à l’échelle mondiale [@noauthor_mit_2018].

Maintenant, considérons la mission de la [[Bibliothèque du Congrès](https://www.loc.gov/){link-archive="https://web.archive.org/web/20190905183245/https://www.loc.gov/"}]{.organisme idsp="Bibliothèque du Congrès" idwiki="https://www.wikidata.org/wiki/Q131454"}&nbsp;:

>La mission centrale de la [Bibliothèque]{.organisme idsp="Bibliothèque du Congrès" idwiki="https://www.wikidata.org/wiki/Q131454"} est de fournir au Congrès, puis au gouvernement fédéral et au peuple américain une source de connaissances riche, diversifiée et durable sur laquelle on peut compter pour les informer, les inspirer et les engager, en plus de soutenir leurs activités intellectuelles et créatives [@noauthor_library_2016].

Prenez note du fait qu’elle définit très clairement sa communauté&nbsp;: le peuple américain, mais seulement après le Congrès et le gouvernement fédéral.

Au bénéfice des parents, des enseignant·e·s, des gestionnaires et de ceux et celles qui s’intéressent à nos écoles, voici quelques énoncés de mission remarquables de bibliothèques scolaires&nbsp;:

>La mission de la [Tehiyah Day School]{.organisme idsp="Tehiyah Day School"} est d’inspirer la curiosité, un fort sentiment de communauté et un lien vibrant avec le judaïsme. À [Tehiyah]{.organisme idsp="Tehiyah Day School"}, nous vivons le programme de formation&nbsp;! [@noauthor_tehiyah_2018]

Autre exemple&nbsp;:

>La mission du programme d’enseignement des médias de la bibliothèque de l’école est&nbsp;:
<ul>
<li> de faire partie intégrale de la [Whittier Elementary School]{.organisme idsp="Whittier Elementary School"} et de sa communauté environnante. </li>
<li> de collaborer avec le personnel afin de créer un véritable apprentissage chez tous les élèves. </li>
<li> de fournir des ressources et un enseignement de qualité aux élèves et au personnel.</li>
<li> d’encourager le personnel et les élèves à utiliser de façon efficace les idées les informations.</li>
<li> de promouvoir la lecture et l’apprentissage tout au long de la vie, tant pour se divertir que pour s’informer [@noauthor_whittier_2018].</li>
</ul>

J’adore tous ces énoncés de mission sans exception. Ils sont la preuve que, peu importe l’institution, la mission peut être énoncée brièvement et être porteuse de sens. Ils peuvent aussi porter sur l’impact que les bibliothèques désirent avoir, et non sur les choses qu’elles collectent.
Ce n’est pas pour rien que ces organisations ont une réputation internationale.

Gardons cela à l’esprit et tournons-nous maintenant vers des missions beaucoup moins inspirantes. J’ai modifié leurs noms par «&nbsp;MaVille&nbsp;» ou «&nbsp;MonUniversité&nbsp;» pour protéger leur identité.

>La bibliothèque publique de MaVille fournit des documents dans divers formats ainsi que des services pour les personnes de tous âges, afin d’aider les résidents de la communauté à obtenir l’information qui répond à leurs besoins personnels, éducatifs et professionnels. Tous les services de la bibliothèque font l’objet d’une promotion active permettant d’accroître la sensibilisation du public et d’améliorer ainsi la qualité de vie des citoyens de MaVille.

Outre le fait que cette mission porte clairement sur ce que la bibliothèque collectionne, il y a un élément ici qui me rend dingue.
Est-ce bien la mission de la bibliothèque de faire la promotion de la bibliothèque&nbsp;? Et non seulement de faire sa propre promotion, mais encore de le faire activement&nbsp;?! N’est-il pas un peu arrogant de dire qu’en sachant que la bibliothèque est là, la vie des citoyen·ne·s s’en trouvera améliorée&nbsp;? Que faut-il donc attendre de cette bibliothèque&nbsp;? Des choses, oui, mais aussi une sorte d’attitude tout à fait nombriliste.

Bon, passons au suivant&nbsp;:

>La mission de la Bibliothèque publique de MaVille est de&nbsp;:
<ul>
<li> Répondre aux besoins récréatifs de ses usagers en soutenant leurs activités de loisirs, et ce, en mettant à leur disposition des documents et des services.</li>
<li> Répondre aux besoins d’informations collectifs et individuels de ses usagers en sélectionnant, acquérant, cataloguant, organisant et distribuant de l’information et des documents.</li>
<li> Assurer l’enrichissement culturel des usagers individuels et de la communauté de MaVille en leur fournissant des documents et des activités connexes qui favorisent la compréhension du développement du patrimoine et de l’évolution du mode de vie à l’échelle internationale, nationale communautaire et individuelle.</li>
<li> Répondre aux besoins en formation continue de ses usagers en soutenant l’apprentissage au-delà de ce qui est requis pour l’obtention de diplômes universitaires ou un emploi en fournissant des documents qui améliorent la vie quotidienne, les intérêts personnels et le rendement au travail.</li>
<li> La Bibliothèque de MaVille reconnaît l’impact de la technologie, en particulier des technologies de l’information et de la communication, sur la communauté de MaVille. La Bibliothèque s’efforce d’identifier, d’obtenir, d’organiser et de donner accès à la technologie sous toutes ses formes. Pour accomplir sa mission, la Bibliothèque de MaVille soutient pleinement les principes de liberté d’expression et de droit d’accès à l’information. La Bibliothèque favorisera une atmosphère de libre investigation et fournira l’information sans parti pris ni discrimination.</li></ul>

Génial&nbsp;! Pouvez-vous imaginer ce message imprimé sur un t-shirt&nbsp;? Ma principale critique ici est qu’il s’agit de mettre en avant les documents sans chercher à en partager la propriété ou la création avec la communauté. La bibliothèque est une servante plutôt qu’un service.
Ceci témoigne d’un autre aspect intéressant de la vision du monde qui différencie la bibliothéconomie ancienne de la bibliothéconomie nouvelle, à savoir la relation de la bibliothèque avec sa communauté.

L’expression «&nbsp;les bibliothèques “&nbsp;pour le peuple&nbsp;”&nbsp;» est une ancienne façon de considérer les bibliothèques. La bibliothèque y est présentée comme une entité séparée de la communauté, un service que cette dernière peut utiliser et payer, mais, qu’au final, elle peut ignorer ou rejeter. La nouvelle vision est plutôt celle d’une bibliothèque «&nbsp;du peuple&nbsp;». La communauté fait partie intégrante de ce que fait la bibliothèque et les bibliothécaires sont des membres à part entière de la communauté. Les bibliothécaires font leur travail non pas parce qu’ils et elles sont «&nbsp;au service&nbsp;» de celle-ci ou parce qu’ils et elles développent un produit destiné à être consommé par la communauté, mais plutôt pour rendre la communauté meilleure. Les membres de la communauté ne soutiennent pas la bibliothèque parce qu’ils et elles sont une clientèle satisfaite, mais parce que la bibliothèque fait partie de ce qu’ils et elles sont.

Cette façon de concevoir les bibliothèques est analogue au fonctionnement démocratique. Lorsque le peuple se sent partie intégrante du gouvernement, ses opinions sont représentées, sa voix est entendue et il se gouverne lui-même (gouvernement «&nbsp;du peuple&nbsp;»). Quand, en revanche, les individus ont l’impression que le gouvernement est une sorte de classe politique à part, l’insatisfaction survient (ce qui, à l’extrême, peut aboutir à un printemps arabe). Les bibliothèques doivent être «&nbsp;du peuple&nbsp;», pas «&nbsp;pour le peuple&nbsp;». Lorsqu’un membre de la communauté entre dans une bibliothèque (ou visite son site Web), il ou elle doit y voir l’occasion de contribuer, de donner son avis et d’améliorer l’institution. Sinon, la bibliothèque n’est qu’une grande librairie commerciale de plus, c’est-à-dire un fournisseur de contenus destiné à être remplacé ou supplanté par la concurrence.

De même, les bibliothécaires cherchent à offrir un excellent service non pas seulement en raison d’un élan altruiste, mais parce qu’ils et elles désirent égoïstement améliorer leur propre condition. Si les bibliothécaires font bien leur travail, la communauté en tirera profit, et une communauté qui se porte bien va en retour améliorer la situation des bibliothécaires. C’est un cercle vertueux.

Intéressons-nous à quelques missions décourageantes de bibliothèques universitaires&nbsp;:

>La Bibliothèque universitaire contribue à la mission académique de MonUniversité en offrant, présentant et préservant une grande variété de ressources informationnelles. Nous utilisons des approches innovantes en collaboration avec le corps professoral et les étudiants pour les aider à découvrir, utiliser, organiser et partager la gamme d’informations qui appuient leurs besoins de recherche, d’enseignement et d’apprentissage.

Pour être honnête, cette mission n’est pas trop flagrante, mais il s’agit encore une fois de soutenir une institution en lui fournissant du matériel (des ressources informationnelles). Aussi, bien que l’innovation soit positive, elle ne concerne dans ce cas que les fonctions de la bibliothèque. Il ne s’agit pas d’aider les innovateurs et les innovatrices ou de favoriser l’innovation à l’intérieur de la communauté. Par ailleurs, cette mission avance que le corps professoral et les étudiant·e·s pourront s’améliorer en travaillant avec la bibliothèque, mais pas que la bibliothèque peut apprendre de (et préférablement avec) la communauté.

En voici une autre&nbsp;:

>La mission de la Bibliothèque de MonUniversité est d’appuyer la recherche et les besoins académiques de son corps professoral et de ses étudiants en fournissant une superbe collection de documents juridiques et en offrant le meilleur niveau de service possible. Dans les limites du possible, la Bibliothèque veut pourvoir aux besoins de recherche de la communauté élargie de MonUniversité ainsi qu’aux chercheurs provenant de l’extérieur de la communauté de MonUniversité qui souhaitent accéder à ses remarquables collections.

En d’autres mots, venez chercher votre matériel ici, car notre matériel est formidable.

Terminons ce tour d’horizon avec un autre exemple de bibliothèque publique&nbsp;:

>La Bibliothèque de MaVille, un organisme au service du public, a pour objectif de fournir à tous les résidents de MaVille une collection complète de documents dans une variété de médias où sont enregistrées les connaissances, les idées et la culture de l’homme. Elle a aussi pour but d’organiser ces documents pour en faciliter l’accès ainsi que d’offrir conseils et encouragements à les utiliser. Une importance particulière est accordée aux documents populaires dans tous les formats et pour tous les âges et à la création d’un centre d’apprentissage et d’éducation tout au long de la vie pour tous les résidents de la communauté. La Bibliothèque sert surtout de lieu où les enfants découvrent la joie de lire et la valeur des Bibliothèques.

Par où commencer avec cet exemple de fond de tiroir, qui n’en a que pour le matériel&nbsp;? Pourquoi pas en remettant en question la possibilité même qu’une bibliothèque puisse posséder une collection complète des connaissances, des idées et de la culture de l’homme&nbsp;? Ce sont des promesses exagérées impossibles à satisfaire. Ajoutons à cela leur volonté de servir tout le monde, tout en endoctrinant nos enfants au passage.

## Une mission fondée sur des attentes plus élevées

Les bibliothèques sont donc en mission, elles doivent améliorer la société en facilitant la création de connaissances. Bien entendu, cette mission les distingue d’autres formes d’institutions. La mission de la bibliothèque s’inscrit presque toujours dans le cadre de la mission d’une organisation plus grande. Une bibliothèque publique fait partie d’une ville ou d’un comté. Une bibliothèque de recherche fait partie d’un collège ou d’une université. Les bibliothèques scolaires sont là pour propulser la mission globale d’une école. Les bibliothèques corporatives sont là pour contribuer à la rentabilité économique de leur entreprise.

Nous reviendrons sur la manière dont la mission d’amélioration de la société est finalement façonnée et façonne une communauté lorsque je discuterai de ce que j’entends exactement par «&nbsp;améliorer la société&nbsp;» au [Chapitre 5](chapitre5.html). Pour l’instant, passons à la préoccupation plus immédiate de la façon dont les bibliothèques remplissent leur mission.
C’est-à-dire, d’abord et avant tout, à la question «&nbsp;qu’est-ce que fait une bibliothèque&nbsp;?&nbsp;» Le savoir est d’autant plus important qu’une bibliothèque ne se contente pas de collectionner des livres.

[^NG]: [L'ensemble des archives du magazine mensuel *National Geographic*, depuis sa première parution en 1888, sont disponibles en ligne mais uniquement sur abonnement payant. [Consulter le site](https://archive.nationalgeographic.com/){link-archive="https://web.archive.org/web/20190905183322/https://archive.nationalgeographic.com/dynamic/National%20Geographic%20Society/National%20Geographic/Landing.aspx"}.]{.editeur}

[^57]: Ces données compilées sont tirées des sources suivantes: [-@noauthor_statistical_1994]&nbsp;; [-@wright_bowker_1956]&nbsp;; [-@noauthor_world_1883].


## Contenus additionnels

!contenuadd(./lankesChapter3EnPdf)(pdf)

!contenuadd(./videoLankesChapter3)(video)


## Références

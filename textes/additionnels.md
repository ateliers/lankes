# AUTEUR

## siteLankes

---
title: >-
  Site personnel de R. David Lankes&nbsp;: travaux et parcours de l'auteur
credits: >-
  R. David Lankes
keywords: lankes,web,site
lang: en
type: pageWeb
link: https://davidlankes.org/
link-archive: https://web.archive.org/web/20190910140934/https://davidlankes.org/
embed:
zotero:
date:
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: main
---


## vimeoLankes

---
title: >-
  Conférences, présentations et décryptages de R. David Lankes sur sa chaîne vidéo
credits: >-
  R. David Lankes
keywords: lankes,video,vimeo
lang: en
type: pageWeb
link: https://vimeo.com/rdlankes
link-archive: https://web.archive.org/web/20190910141040/https://vimeo.com/rdlankes
embed:
zotero:
date:
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: main
---


# LIVRE

## playlistDeconstructed

---
title: >-
  «&nbsp;Deconstructed&nbsp;»&nbsp;: la version originale de l'ouvrage décrypté chapitre par chapitre, en vidéo et en anglais, par son auteur
credits: >-
  R. David Lankes
keywords: anglais,lankes,video,vimeo,decryptage
lang: en
type: video
link:
link-archive:
embed:
zotero:
date:
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: main
---


## expectMorePdf

---
title: >-
  *Expect more. Demanding better libraries for today's complex world*&nbsp;: version originale en anglais de l'ouvrage, téléchargeable gratuitement au format PDF
credits: >-
  R. David Lankes
keywords: anglais,lankes,pdf,livre
lang: en
type: pdf
link: ./media/ExpectMore2.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: main
---



## conferenceCbpq

---
title: >-
  «&nbsp;"Expect more": Soyons des activistes plutôt que des agents de changement&nbsp;»&nbsp;: retransmission vidéo de la conférence donnée en anglais par R. David Lankes le 21 septembre 2018 pour le CBPQ. (2018, 19min11s)
credits: >-
  R. David Lankes, CBPQ (Corporation des bibliothécaires professionnels du Québec), asted/FMD
keywords: anglais,lankes,video,conference,CBPQ,montreal,quebec,asted,FMD
lang: en
type: video
link: https://youtu.be/HYP36RNWDeY
link-archive:
embed: https://www.youtube.com/embed/HYP36RNWDeY
zotero:
date: 2018-09-21
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: main
---



# CHAPITRE 1

## lankesChapter1enPdf

---
title: >-
  Version originale en anglais du chapitre 1&nbsp;: «&nbsp;The Arab Spring: Expect the Exceptional&nbsp;»
credits: >-
  R. David Lankes
keywords: version originale,vo,anglais,lankes,pdf,printemps arabe
lang: en
type: pdf
link: ./media/ExpectMore2Chap1.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).



## videoLankesChapter1

---
title: >-
  Déconstruction du chapitre 1&nbsp;: «&nbsp;Le printemps arabe&nbsp;: exigeons l’exceptionnel&nbsp;» (2016, 12min28s)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes
lang: en
type: video
link: https://vimeo.com/202536052
link-archive: https://web.archive.org/web/20190910141424/https://vimeo.com/202536052
embed: https://player.vimeo.com/video/202536052
zotero:
date: 2016
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 1 «&nbsp;Le printemps arabe&nbsp;: exigeons l’exceptionnel&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.





# CHAPITRE 2

## lankesChapter2EnPdf

---
title: >-
  Version originale en anglais du chapitre 2&nbsp;: «&nbsp;The	Argument for	Better	Libraries:	Expect	Impact&nbsp;»
credits: >-
  R. David Lankes
keywords: version originale,vo,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap2.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).


## videoLankesChapter2

---
title: >-
  Déconstruction du chapitre 2&nbsp;: «&nbsp;De meilleures bibliothèques pour plus d’impact social&nbsp;» (2017, 21min10s)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/203608818
link-archive: https://web.archive.org/web/20190910141145/https://vimeo.com/203608818
embed: https://player.vimeo.com/video/203608818
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 2 «&nbsp;De meilleures bibliothèques pour plus d’impact social&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.


## artDeweyBbf

---
title: >-
  En retraçant le parcours de Melvil Dewey, Annie Béthery s'interroge sur le succès de sa classification et sur les usages qui en sont fait aujourd'hui.
credits: >-
  Annie Béthery, Bulletin des Bibliothèques de France (BBF)
keywords: article,français,Melvil Dewey,Dewey,BBF,Bulletin des Bibliothèques de France,Melvil Dewey
lang: fr
type: article
link: http://bbf.enssib.fr/consulter/bbf-2012-01-0022-004
link-archive: https://web.archive.org/web/20190617160324/http://bbf.enssib.fr/consulter/bbf-2012-01-0022-004
embed:
zotero: '@bethery_melvil_2012'
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## artDeweySavoirsCdi

---
title: >-
  Marie-France Blanquet dresse le portrait du père de la bibliothéconomie moderne à travers grandes dates et mots-clés.
credits: >-
  Marie-France Blanquet, Savoirs CDI
keywords: article,français,melvil dewey,melville dewey,dewey,savoirs CDI,marie-france blanquet
lang: fr
type: article
link: https://www.reseau-canope.fr/savoirscdi/societe-de-linformation/le-monde-du-livre-et-de-la-presse/histoire-du-livre-et-de-la-documentation/biographies/melville-dewey-pere-de-la-bibliotheconomie-moderne.html
link-archive: https://web.archive.org/web/20190617184639/https://www.reseau-canope.fr/savoirscdi/societe-de-linformation/le-monde-du-livre-et-de-la-presse/histoire-du-livre-et-de-la-documentation/biographies/melville-dewey-pere-de-la-bibliotheconomie-moderne.html
embed:
zotero: '@blanquet_melville_2006'
date: 2006
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## refDeweyWiegand

---
title: >-
  Biographie sans concession de Melvil Dewey par l'historien des bibliothèques Wayne A. Wiegand
credits: >-
  Wayne A. Wiegand, ALA Editions
keywords: livre,anglais,melvil dewey,ALA,american library association,wayne A. wiegand,biographie
lang: en
type: article
link: https://www.alastore.ala.org/content/irrepressible-reformer-biography-melvil-dewey
link-archive: https://web.archive.org/save/https://www.oclc.org/fr/dewey/resources/biography.html
embed:
zotero: '@wiegand_irrepressible_1996'
date: 1996
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## dossierDewey

---
title: >-
  Melvil Dewey
credits:
keywords: dossier,article,biographie,melvil dewey,melville dewey,dewey,BBF,bulletin des bibliothèques de france,annie béthery,Wayne a. wiegand,marie-france blanquet,savoirs cdi
linkedTo: >-
  ![contenuadd](./artDeweyBbf)(article) ![contenuadd](./artDeweySavoirsCdi)(article) ![contenuadd](./refDeweyWiegand)(article)
lang: fr
type: dossier
link:
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---

Deux articles et un ouvrage de référence pour cerner la personnalité controversée du bibliothécaire américain, créateur de la Classification décimale de Dewey.



## imgSanGiorgioPistoia

---
title: >-
  Hall d'entrée de la Bibliothèque San Giorgio à Pistoia (Toscane, Italie)
credits: >-
  Cristina Bambin [[CC BY-SA 2.0 it](https://creativecommons.org/licenses/by-sa/2.0/it/deed.en)]
keywords: image,pistoia,toscane,italie,bibliothèque san giorgio,wikimedia
lang: fr
type: image
link: ./media/BibliotecaPubblicaSanGiorgioPistoia.jpeg
link-archive: https://upload.wikimedia.org/wikipedia/commons/9/98/Biblioteca_pubblica._San_Giorgio_di_Pistoia.JPG
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: side
---


## imgBloorReferenceLibrary

---
title: >-
  Cabines en verre de la Bloor Reference Library à la Toronto Public Library (Ontario, Canada)
credits: >-
  [Toronto Public Library](https://www.torontopubliclibrary.ca/using-the-library/study-space.jsp)
keywords: image,bloor reference library,toronto public library,study space
lang: fr
type: image
link: ./media/BloorReferenceLib-StudySpace.jpg
link-archive:
embed:
zotero:
date:
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: side
---


## artTroisiemeLieuServet

---
title: >-
  Mathilde Servet explore une nouvelle génération d'établissements culturels&nbsp;: les bibliothèques troisième lieu.
credits: >-
  Mathilde Servet, Bulletin des bibliothèques de France (BBF)
keywords: article,français,mathilde servet, troisieme lieu,bibliotheque,concept,bbf,bulletin des bibliotheques de france
lang: fr
type: article
link: http://bbf.enssib.fr/consulter/bbf-2010-04-0057-001
link-archive: https://web.archive.org/web/20190502071536/http://bbf.enssib.fr/consulter/bbf-2010-04-0057-001
embed:
zotero:
date: 2010
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---


## conceptTroisiemeLieu

---
title: >-
  Marie D. Martel revient sur le concept de bibliothèque troisième lieu.
credits: >-
  Marie D. Martel
keywords: marie d. martel,bibliotheque,troisieme lieu,concept,glossaire
lang: fr
type: pageWeb
link: './glossaire.html#bibliothèque-troisième-lieu'
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: comite
priority: lowpriority
position: main
---

## conceptLitteratie

---
title: >-
  Marie D. Martel revient sur le concept de littératie.
credits: >-
  Marie D. Martel
keywords: marie d. martel,litteratie,concept,glossaire
lang: fr
type: pageWeb
link: './glossaire.html#littératie'
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: comite
priority: lowpriority
position: main
---


# CHAPITRE 3

## lankesChapter3EnPdf

---
title: >-
  Version originale en anglais du chapitre 3&nbsp;: «&nbsp;The Mission of Libraries: Expect More Than Books&nbsp;»
credits: >-
  R. David Lankes
keywords: vo,version originale,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap3.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).




## videoLankesChapter3

---
title: >-
  Déconstruction du chapitre 3&nbsp;: «&nbsp;La mission des bibliothèques&nbsp;: bien plus que des livres&nbsp;» (2017, 12min41s)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/203831886
link-archive: https://web.archive.org/web/20190910141732/https://vimeo.com/203831886
embed: https://player.vimeo.com/video/203831886
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 3 «&nbsp;La mission des bibliothèques : bien plus que des livres&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.



## videoBiblioAlexandrie

---
title: >-
  Elizabeth Cox retrace l'histoire, de la création à sa chute, de la bibliothèque d'Alexandrie.
credits: >-
  Elizabeth Cox - TED-Ed
keywords: video,youtube,anglais,TED,TED-Ed,Elizabeth Cox,library of alexandria,bibliothèque d'alexandrie
lang: en
type: video
link: https://youtu.be/jvWncVbXfJ0
link-archive:
embed: https://www.youtube.com/embed/jvWncVbXfJ0
zotero:
date: 2018
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---




## artRanganathanEnssib

---
title: >-
  Le professeur K. S. Raghavan retrace la vie du bibliothécaire prodigue aux idées révolutionnaires.
credits: >-
  K. S. Raghavan (traduit de l’anglais par Anne Métivier et Jérome Mattio, Médiathèque de Hyères), Bibliothèque(s) - Revue de l'association des bibliothèques de France
keywords: article,français,S. R. Ranganathan,K. S. Raghavan,bibliothèque(s),association des bibliothécaires de france,enssib
lang: fr
type: article
link: https://www.enssib.fr/bibliotheque-numerique/documents/59208-31-inde.pdf#page=32
link-archive: http://web.archive.org/web/20190408182657/https://www.enssib.fr/bibliotheque-numerique/documents/59208-31-inde.pdf#page=32
embed:
zotero: '@raghavan_shiyalia_2007'
date: 2007
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## artRanganathanBbf

---
title: >-
  À la mort du bibliothécaire, Éric de Grolier offre une vue d'ensemble de l’œuvre et des contributions du «&nbsp;Maître&nbsp;».
credits: >-
  Éric de Grolier, Bulletin des Bibliothèques de France (BBF)
keywords: article,français,S. R. Ranganathan,BBF,Bulletin des Bibliothèques de France,Éric de Grolier
lang: fr
type: article
link: http://bbf.enssib.fr/consulter/bbf-1973-05-0203-003
link-archive: http://web.archive.org/web/20190408183549/http://bbf.enssib.fr/consulter/bbf-1973-05-0203-003
embed:
zotero: '@grolier_shiyali_1973'
date: 1973
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## artRanganathanSavoirsCdi

---
title: >-
  Marie-France Blanquet dresse un portrait clair et complet de Shiyali Ramamrita Ranganathan.
credits: >-
  Marie-France Blanquet, Savoirs CDI
keywords: article,français,S. R. Ranganathan,savoirs CDI,marie-france blanquet
lang: fr
type: article
link: https://www.reseau-canope.fr/savoirscdi/index.php?id=526
link-archive: http://web.archive.org/web/20190408194352/https://www.reseau-canope.fr/savoirscdi/index.php?id=526
embed:
zotero: '@blanquet_shiyali_2007'
date: 2007
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## dossierRanganathan

---
title: >-
  S. R. Ranganathan
credits:
keywords: dossier,article,S. R. Ranganathan,savoirs CDI,marie-france blanquet,BBF,bulletin des bibliothèques de france,éric de grolier,K. S. Raghavan,bibliothèque(s),association des bibliothécaires de france,enssib
linkedTo: >-
  ![contenuadd](./artRanganathanEnssib)(article) ![contenuadd](./artRanganathanBbf)(article) ![contenuadd](./artRanganathanSavoirsCdi)(article)
lang: fr
type: dossier
link:
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---

Trois articles qui reviennent sur le parcours et les apports du bibliothécaire indien au monde de la bibliothéconomie, dont la classification à facettes et les cinq lois qui portent son nom.


# CHAPITRE 4

## lankesChapter4EnPdf

---
title: >-
  Version originale en anglais du chapitre 4&nbsp;: «&nbsp;Facilitating Knowledge Creation: Expect To Create&nbsp;»
credits: >-
  R. David Lankes
keywords: vo,version originale,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap4.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).


## videoLankesChapter4

---
title: >-
  Déconstruction du chapitre 4&nbsp;: «&nbsp;Faciliter la création de connaissances&nbsp;» (2017, 15min59s)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/204179315
link-archive: https://web.archive.org/web/20190910141850/https://vimeo.com/204179315
embed: >-
  https://player.vimeo.com/video/204179315
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 4 "Faciliter la création de connaissances" de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.


## imgMaslow

---
title: >-
  Pyramide des besoins selon Abraham Maslow
credits: >-
  Les Ateliers de [sens public]
keywords: image,pyramide des besoins,pyramide de Maslow,Maslow's hierarchy of needs
lang: fr
type: image
link: ./media/PyramideDeMaslow.jpg
link-archive:
embed:
zotero:
date:
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

## conceptConversation

---
title: >-
  Jean-Michel Lapointe revient sur le concept de conversation.
credits: >-
  Jean-Michel Lapointe
keywords: jean-michel lapointe,concept,glossaire,conversation
lang: fr
type: pageWeb
link: './glossaire.html#conversation'
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: comite
priority: lowpriority
position: main
---


# CHAPITRE 5

## lankesChapter5EnPdf

---
title: >-
  Version originale en anglais du chapitre 5&nbsp;: «&nbsp;Improve Society: Expect Grander&nbsp;»
credits: >-
  R. David Lankes
keywords: vo,version originale,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap5.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).



## videoLankesChapter5

---
title: >-
  Déconstruction du chapitre 5&nbsp;: «&nbsp;Améliorer la société&nbsp;» (2017, 11min15s)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/205895978
link-archive: https://web.archive.org/web/20190910142006/https://vimeo.com/205895978
embed: https://player.vimeo.com/video/205895978
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 5 «&nbsp;Améliorer la société&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.


## habitsNprPodcast

---
title: >-
  Écouter l'intégralité de l'interview de Charles Duhigg par Terry Gross (38min10s).
credits: >-
  Fresh Air par Terry Gross, NPR
keywords: podcast,anglais,Terry Gross,Fresh Air,Charles Duhigg,Habits,interview,son,NPR
lang: en
type: video
link: https://www.npr.org/2012/03/05/147192599/habits-how-they-form-and-how-to-break-them
link-archive: https://web.archive.org/web/20191009155934/https://www.npr.org/2012/03/05/147192599/habits-how-they-form-and-how-to-break-them
embed: https://www.npr.org/player/embed/147192599/147954447
zotero: '@gross_habits:_nodate'
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---

## conceptConnaissance

---
title: >-
  Jean-Michel Lapointe revient sur le concept de connaissance.
credits: >-
  Jean-Michel Lapointe
keywords: jean-michel lapointe,connaissance,concept,glossaire
lang: fr
type: pageWeb
link: './glossaire.html#connaissance'
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: comite
priority: lowpriority
position: main
---

# CHAPITRE 6

## lankesChapter6EnPdf

---
title: >-
  Version originale en anglais du chapitre 6&nbsp;: «&nbsp;Communities: Expect a Platform&nbsp;»
credits: >-
  R. David Lankes
keywords: vo,version originale,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap6.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).


## videoLankesChapter6

---
title: >-
  Déconstruction du chapitre 6&nbsp;: «&nbsp;Faire communauté&nbsp;» (2017, 26min45s)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/209009364
link-archive: https://web.archive.org/web/20190910142042/https://vimeo.com/209009364
embed: >-
  https://player.vimeo.com/video/209009364
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 6 «&nbsp;Faire communauté&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.



## artDeweyBbf

---
title: >-
  En retraçant le parcours de Melvil Dewey, Annie Béthery s'interroge sur le succès de sa classification et sur les usages qui en sont fait aujourd'hui.
credits: >-
  Annie Béthery, Bulletin des Bibliothèques de France (BBF)
keywords: article,français,Melvil Dewey,Dewey,BBF,Bulletin des Bibliothèques de France,Melvil Dewey
lang: fr
type: article
link: http://bbf.enssib.fr/consulter/bbf-2012-01-0022-004
link-archive: https://web.archive.org/web/20190617160324/http://bbf.enssib.fr/consulter/bbf-2012-01-0022-004
embed:
zotero: '@bethery_melvil_2012'
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## artDeweySavoirsCdi

---
title: >-
  Marie-France Blanquet dresse le portrait du père de la bibliothéconomie moderne à travers grandes dates et mots-clés.
credits: >-
  Marie-France Blanquet, Savoirs CDI
keywords: article,français,melvil dewey,melville dewey,dewey,savoirs CDI,marie-france blanquet
lang: fr
type: article
link: https://www.reseau-canope.fr/savoirscdi/societe-de-linformation/le-monde-du-livre-et-de-la-presse/histoire-du-livre-et-de-la-documentation/biographies/melville-dewey-pere-de-la-bibliotheconomie-moderne.html
link-archive: https://web.archive.org/web/20190617184639/https://www.reseau-canope.fr/savoirscdi/societe-de-linformation/le-monde-du-livre-et-de-la-presse/histoire-du-livre-et-de-la-documentation/biographies/melville-dewey-pere-de-la-bibliotheconomie-moderne.html
embed:
zotero: '@blanquet_melville_2006'
date: 2006
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## refDeweyWiegand

---
title: >-
  Biographie sans concession de Melvil Dewey par l'historien des bibliothèques Wayne A. Wiegand
credits: >-
  Wayne A. Wiegand, ALA Editions
keywords: livre,anglais,melvil dewey,ALA,american library association,wayne A. wiegand,biographie
lang: en
type: article
link: https://www.alastore.ala.org/content/irrepressible-reformer-biography-melvil-dewey
link-archive: https://web.archive.org/save/https://www.oclc.org/fr/dewey/resources/biography.html
embed:
zotero: '@wiegand_irrepressible_1996'
date: 1996
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---


## dossierDewey

---
title: >-
  Melvil Dewey
credits:
keywords: dossier,article,biographie,melvil dewey,melville dewey,dewey,BBF,bulletin des bibliothèques de france,annie béthery,Wayne a. wiegand,marie-france blanquet,savoirs cdi
linkedTo: >-
  ![contenuadd](./artDeweyBbf)(article) ![contenuadd](./artDeweySavoirsCdi)(article) ![contenuadd](./refDeweyWiegand)(article)
lang: fr
type: dossier
link:
link-archive:
embed:
zotero:
date: 2019
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: side
---

Deux articles et un ouvrage de référence pour cerner la personnalité controversée du bibliothécaire américain, créateur de la Classification décimale de Dewey.


## imgMansuetoLibrary

---
title: >-
  Dôme vitré de la Joe and Rika Mansueto Library de l'Université de Chicago
credits: >-
  [David](https://www.flickr.com/photos/118501611@N02/16370484016/), Flickr [[CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)]
keywords: image,flickr,joe and rika mansueto library,chicago,universite de chicago,dome,architecture,bibliotheque
lang: fr
type: image
link: ./media/JoeAndRikaMansuetoLibrary.jpg
link-archive: https://flic.kr/p/qWB1yq
embed:
zotero:
date: 2015
date-publication: 2019-10-28
source: editeur
priority: highpriority
position: side
---


# CHAPITRE 7

## lankesChapter7enPdf

---
title: >-
  Version originale en anglais du chapitre 7&nbsp;: «&nbsp;Librarians: Expect Brilliance&nbsp;»
credits: >-
  R. David Lankes
keywords: vo,version originale,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap7.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).



## videoLankesChapter7

---
title: >-
  Déconstruction du chapitre 7&nbsp;: «&nbsp;Des bibliothécaires de génie&nbsp;» (2017, 13min)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/210328445
link-archive: https://web.archive.org/web/20190910142255/https://vimeo.com/210328445
embed: https://player.vimeo.com/video/210328445
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 7 «&nbsp;Des bibliothécaires de génie&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.



# CHAPITRE 8

## lankesChapter8enPdf

---
title: >-
  Version originale en anglais du chapitre 8&nbsp;: «&nbsp;Action Plan: Expect More&nbsp;»
credits: >-
  R. David Lankes
keywords: vo,version originale,anglais,lankes,pdf
lang: en
type: pdf
link: ./media/ExpectMore2Chap8.pdf
link-archive:
embed:
zotero:
date: 2012
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Cet ouvrage a été écrit et publié en anglais par R. David Lankes (2012, Jamesville, NY: Riland Publishing) avant d'être traduit collectivement, sous la direction de Jean-Michel Lapointe, en français en 2018 (Les Ateliers de [sens public], Montréal).



## videoLankesChapter8

---
title: >-
  Déconstruction du chapitre 8&nbsp;: «&nbsp;Un plan d'action&nbsp;: exigeons de meilleures bibliothèques&nbsp;» (2017, 13min)
credits: >-
  R. David Lankes
keywords: video,vimeo,anglais,lankes,decryptage
lang: en
type: video
link: https://vimeo.com/210342017
link-archive: https://web.archive.org/web/20190910142427/https://vimeo.com/210342017
embed: https://player.vimeo.com/video/210342017
zotero:
date: 2017
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Dans cette vidéo, R. David Lankes déconstruit le texte du chapitre 8 «&nbsp;Un plan d'action&nbsp;: exigeons de meilleures bibliothèques&nbsp;» de l'ouvrage *Exigeons de meilleures bibliothèques* et traite de l'utilisation de ce texte au sein d'une conversation plus large sur les bibliothèques et leurs services.

## videoJosephBlonde

---
title: >-
  Introduction à la référence virtuelle coopérative par le bibliothécaire Joseph Blonde (2010, 59min22s)
credits: >-
  Joseph Blonde, BAnQ
keywords: video,banq,reference virtuelle,joseph blonde,journees professionnelles banq,bibliotheque,services de reference electronique
lang: fr
type: video
link: http://collections.banq.qc.ca/ark:/52327/3482710
link-archive: https://web.archive.org/web/20191016164934/http://numerique.banq.qc.ca/patrimoine/details/52327/3482710?docref=LUCHPEDIyGDtigSoZsnL3A
embed: http://numerique.banq.qc.ca/patrimoine/details/52327/3482710?docref=LUCHPEDIyGDtigSoZsnL3A
zotero:
date: 2010-04-09
date-publication: 2019-10-28
source: editeur
priority: lowpriority
position: main
---

Présentation de Joseph Blonde, bibliothécaire à l'Université de Concordia, à l'occasion de la Journée d'échanges sur la référence virtuelle coopérative (BAnQ, 9 avril 2010).

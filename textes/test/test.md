
# Documentation

Les contenus additionnels  sont de plusieurs types (note, image, texte, vidéo, lien, etc.), et chaque type peut se décliner dans une à trois catégories :
1. corps de texte: le contenu s'insère au sein du corps de texte.
2. note : le contenu vient augmenter un élément du corps de texte.
3. contenu additionnel : le contenu vient augmenter la page.

Soit le tableau suivant qui présente comment on appelle les différents types de contenu :

type/catégorie | corps | note | additionnel
:--            |:--    |:--   |:--
image  | `![Légende](image.png)`  | `!image(maClé)(note)`   |  `!image(maClé)(additionnel)`


# Chapitre 1. Le printemps arabe&nbsp;: exigeons l’exceptionnel

Le printemps arabe venait d’éclore en Égypte. Au début de l’an 2011, peu de temps après le succès de la révolution tunisienne, le peuple égyptien descend à son tour dans la rue pour exiger des réformes d’un régime au pouvoir depuis près de trente ans. Alors que l’attention médiatique se concentre surtout sur les protestataires de la place Tahrir au Caire, capitale égyptienne, plusieurs manifestations prennent place dans la ville portuaire d’Alexandrie. Au sein de ces deux villes, des gens de toutes générations provenant de toutes les classes sociales manifestent pour la liberté, la justice et l’équité sociale. Le printemps arabe était perçu, à ses débuts, comme un soulèvement pacifique visant à restaurer la constitution. Ce mouvement a, en revanche, fait plusieurs victimes&nbsp;: on compte au moins 846 mort·e·s et 6 000 blessé·e·s dans toute l’Égypte [^3]. Le 28 janvier, à dix-huit heures, les prisons relâchent dans les rues des personnes condamnées pour meurtre ou crime sexuel&nbsp;; l’insécurité règne à Alexandrie. Des bandes errantes se livrent au pillage et s’approprient les rues, tirant profit du chaos.


[^3]: Eh oui, un bibliothécaire et professeur d'université vient tout juste de citer Wikipédia. Je le ferai à plusieurs reprises dans ce livre. Il n'y a rien de foncièrement mauvais ou de non fiable dans Wikipédia. En fait, le processus de création de l'information de Wikipédia est plus transparent que celui des encyclopédies classiques. Je cite Wikipédia parce que c'est un endroit facile à consulter pour les lecteurs et les lectrices. C'est aussi un bon point de départ pour trouver des références menant à d'autres ouvrages, et j'ai vérifié cette information dans d'autres sources... ce que tout le monde devrait faire.

## Références

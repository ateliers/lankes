# Questions soulevées au fil de l'eau

@ln
1. Définir typologie de nommage des contenus_additionnels (ex:objetAuteur1ermottitre)

    - @nico: Je pense que cette typologie est à la discrétion de l'éditeur ou de l'auteur, car la spécification finale de l'objet sera fait via la macro appelée.


2. Possibilité de faire un sommaire en tête de document pour lister tous les contenus_additionnels?

    - @nico: oui en effet. Plusieurs façons de faire. Soit utiliser un plugin atom qui crée un sommaire automatiquement, et qui se met à jour. Soit faire une recherche avec la Regex `\#\# .*`, cliquer sur "Find All" et faire un copier, puis un coller en début de document (en fait, ca ne marche pas).

3. Quelle langue pour les keywords ?

    - @nico: français si la langue du livre est français. Penses tu à quelque chose en particulier pour poser la question ?

4. Définir une typologie des keywords (ex : type de document, langue, auteur...)

    - @nico: à quel usage ?
    - @ln: pour une créer une certaine unité dans la catégorisation des CA. Guider/simplifier le travail de l'éditeur et de l'auteur. Simplifier les usages autour des CA pour l'éditeur et le lecteur (ex : retrouver tous les CA en anglais, ou toutes les vidéos pour faire une playlist)

5. Traduction des titres en français ?

    - @nico: what is the question ?
    - @ln: je pensais au fait que les contenus additionnels - et leurs titres - peuvent être dans d'autres langues (ex :la vidéo «Chapter 1 Deconstructed: "The Arab Spring: Expect the Exceptional"» (2016, 12min28s)). Mais 1. ce sera sûrement du cas par cas (projet par projet) et 2. on a la possibilité d'expliquer en FR dans le court texte de description qui accompagne/contextualise le CA.

6. Dans quel champ indiquer les infos complémentaires (ex : date, durée - pour vidéo/audio, support - pour oeuvre...) ? Ajouter un ou plusieurs champs dans le yml ?

    - @nico: Je propose que dans un premier temps, tu ajoutes tout champ que tu considères pertinent. Je suggèrerais qu'on utilise des champs en français.

7. Fait-on figurer la bibliographie dans les contenus additionnels ou bien la laisse-t-on en dehors comme un contenu présent sur chaque chapitre au même titre que le résumé, l'auteur, le sommaire... ?

    - @nico: selon moi la bibliographie peut être une section à part, comme le résumé, l'auteur, etc, et non dans les contenus additionnels.

8. Pour les liens Wikipédia : lien permanent vers la version consultée au moment de la publication ou lien vers la page en mouvement? Ex : https://fr.wikipedia.org/w/index.php?title=Printemps_arabe&oldid=151504862 vs https://fr.wikipedia.org/wiki/Printemps_arabe

    - @nico: je n'ai pas d'avis tranché. à discuter avec @servanne et @marcello


## Questions sur la forme

### Notes
9. Est-ce qu'on différencie (et comment) les notes de l'auteur et les notes ajoutées (notes de l'éditeur) (et/ou les notes non présentes dans la version papier) - dans le back et dans le front ?
Option : sourcer les notes pour indiquer leur provenance (auteur, éditeur, traducteur, autre)
Ex : <!-- Note ajoutée - Source : Édito -->

    VOIR PAD 31012019

10. Et quid de la numérotation des notes qui change si ajout de notes ?

    ON SEN FOUT

### Références
11. Pour ajouter des références bibliographiques > avoir accès au Zotero (Sens Public ou Ateliers Sens Public ?)

    TODO

12. Est-ce qu'on différencie (et comment) les références ajoutées par l'éditeur (et/ou les références non présentes dans la version papier) - dans le back et dans le front ?
Option: idem que notes
Ex : <!-- Ref ajoutée - Source : Édito -->

    ON ajoute un mot-clé dans Zotero/bib à chaque référence

13. Intérêt de créer une réf biblio pour un lien seul ?
Ex: lorsqu'un organisme (ou site web) est cité et qu'on souhaite renvoyer sur le site, plus léger un lien externe seul qu'une réf biblio ? Voir par ex. Freegal ou Congressional Research Service dans chap2.

  - type de liens à la discretion de l'éditeur
  - sémantisation corps de texte

### Liens internes
14. Comment signaler les liens internes ? Peut-on les "créer" en .md ou doit-on juste les signaler dans le .md et les créer après (comment) ?

  [mon texte](#mon titre) voir DOCU

### Modification du texte
15. Comment signaler les erreurs relevées sur la forme (typo, trad, texte coupé...) ?
Ex: Insertion d'un comment dans le corps du texte de type <!-- Typo : blablabla -->

    OK

### Balisage du texte
16. Utiliser cette syntaxe [terme à baliser]{.nom de la balise} ?

    OK + id autorité (éventuellement)

### Fichiers Additionnels et Illustrations
17. Un par chapitre ou un par livre ?

    Un par Livre OK > impacte le script de build.

### Commit
18. Le message Commit (commit -m) doit-il avoir un sens pour tous ?

    OUI! ex: Chapitre1 - rev. linguistique : suppression de tous les ...

## Questions sur le fond

### Balisage du texte
19. Choix des termes à baliser et syntaxe à définir:
* les bibliothèques, universités et organismes {.organisme}
* les sites, services, bases de données et catalogues en ligne {.site}
* les personnes {.personnalite}
* éventuellement lieu {.lieu}

    OK

20. Si le terme est répété, on le balise à chaque fois ?

    1 fois par page web/url ?

    Si on a un index, il y aura un travail postérieur pour 'ancrer' les diff. occurences du terme. L'occurence alignée sur une autorité sera utilisé comme référence pour l'index.

21. Liens externes liés aux termes balisés (renvoi bio, site...) en direct dans le texte ?

    cf ci-dessus point 13

### Annotations
22. Intègre-t-on, et si oui comment, les annotations déjà faites sur le pdf en ligne ?

    Réintégrées annotations dans des notes "sourcées" comme "annotation".

## Salves de questions #3

23. Intérêt et mode opératoire de la création de "dossier" dans les CA ? Un "dossier" étant ici un regroupement d'un (petit) ensemble de ressources permettant d'approfondir un même sujet.
Ex : S.R. Ranganathan - Il est cité dans le chap3 et est une personnalité importante en bibliothéconomie. J'ai sous le coude quelques articles sur lui (2-3) qui pourraient constituer un dossier à mettre en CA.
Est-ce une bonne idée ? Si oui comment la formaliser dans le fichier additionnels.md. Pour le moment, j'ai listé chaque document comme un CA puis créé un CA dossier.

24. CA et ref biblio. Quand un CA peut être une ref biblio (article, ouvrage), je les ajoute dans la bibliographie et je mets la ref dans additionnels.md.

## Salves de questions #4

### Questions
- Peut-on mettre un idORCID pour les personnalités quand il n'existe pas d'autorité sur Rameau ou Wikidata (Ex : membres du comité) (`[Jean-Michel Lapointe]{.personnalite idsp="Jean-Michel Lapointe" idorcid="XXX-XX-XX-XX"`)?
  - @nicolas: Oui, sans pb.
  - @ln: Ok parfait, c'est fait et ajouté dans la Doc.
- Quid de la gestion des espaces insécables ? Est-ce qu'il faut faire quelque chose ou juste go with the flow ? (Ex : intro "établissements homologues" avec le guillemet tout seul).
  - @nicolas: normalement, il faudrait faire qlq chose en effet, mais je suggère de faire une passe automatique en fin de processus.
  - @ln: Ok, je note dans les choses à faire à la fin.
- Quid de l'exploitation des infos de la bibliographie structurée : liens archivés (dans champ Archive ou Extra), marqueurs (accès libre + source - éditeur, auteur, annotation) ?
  - @nicolas: parles-tu d'un contenu additionnel de type "bibliographie" ? Ou bien d'informations existante dans le bibtex que l'on souhaiterait afficher ?
  - @ln: Je parle d'informations existantes dans Zotero (dans les champs Archive ou Extra et dans les Marqueurs) - mais qui n'apparaissent pas dans le BibTex.
- Quand on clique sur une référence - et qu'on arrive en bas dans Références - y a-t-il moyen de pouvoir revenir directement à la lecture ?
  - @nicolas: non actuellement. on le fait sur SP, mais c'est du javascript un peu lourd. ca serait relativement facile si il y avait une correspondance unique entre un appel de citation et la référence. Mais ca n'est pas le cas. Ça demande alors de parser le texte, comptabiliser les appels pour chaque référence, ajouter des ancres, etc.
  - @ln: ça fluidiferait grandement la lecture. Sans retour c'est vraiment pénible. Je comprends que ce ne soit pas possible pour un temps 1 mais je le garderai en tête pour un temps 2.
- Est-ce que je dois créer les pages Index et Glossaire ?
  - @nicolas: non pour la page index, c'est de mon ressort. Où en sont-ils du glossaire ? quelle forme cela prend ? On va voir en fonction de ça je pense.
  - @ln: j'ai rien pour le moment sur le glossaire. Je vais les relancer.
- Sens de la médiathèque dans Sommaire général ? Si on décide de l'enlever, il faut l'enlever du yaml.
  - @nicolas: je n'ai pas d'avis. l'idée était de lister tous les médias ? je pense que si c'est pour les afficher comme dans les chapitres ca ne sert à rien. Si on propose une autre éditorialisation, ca peut commencer à être intéressant, mais je ne pense pas qu'on ait le temps de le faire là.
  - @ln: je vais l'enlever pour le 28 octobre. Pas d'intérêt en l'état. On la rajoutera si besoin plus tard.

### Problèmes
- Point d'arrivée des ancres : arrive plus bas que le positionnement de l'ancre (cf exemple Préface comité) - le header cache le point d'arrivée. Idem pour les renvois aux références bibliographiques, elles sont toujours cachées par le header.
  - @nicolas: c'est une issue pour David.
- Les ancres inter-chapitres ne fonctionnent pas (chapitre4.html#fablab).
  - @nicolas: en effet, les ids que tu mets sont préfixés systématiquement avec l'id du chapitre : preface, introduction, chapitre1, etc. Donc tu ne touches pas aux ancres elles sont correctes, par contre il te faut anticiper le lien vers l'ancre ainsi : `[mon lien](chapitre4.html#chapitre4-fablab)`
  - @ln: Oui ok, ça fonctionne comme ça, je mets tout à jour.
- Les notes en marge sont décalées (pas alignées avec les autres notes) quand l'appel de note est dans une citation.
  - @nicolas: issue pour David.
- Les contenus additionnels inline n'apparaissent pas toutes sur process-local (cf Chap2 - Dewey et Classification Dewey).
  - @nicolas: je les ai chez moi. pourrais tu m'en pointer un en particulier ?
  - @ln: nécessité de mettre à jour lankes-html pour avoir les CA à jour.


## Salves de questions #5

### Affichage
- Légendes des images ferrées à droite : prennent bcp de place et déplace le texte en cassant la lecture. Pas beau.
- Revoir l'affichage des titres de niveau 4 (très petits). Cf Chapitre 4 - *La sécurité physique* et *La sécurité intellectuelle*.
- Affichage des citations longues : seul le décalage du texte indique qu'il s'agit d'une citation. Un peu léger, pas très visible.
- Réflexion sur l'affichage des références bibliographiques : en bas de page ou dans la colonne de droite ? Mais quid quand nombreuses ref à la suite ? Et quel style (avec Chicago, affichage des liens archivées en +, prend bcp de place)?

### Contenus additionnels
Suite à nos derniers échanges, j'ai modifié pas mal de choses dans les contenus additionnels. Il faudrait si possible mettre à jour lankes-html sur lequel travaille David pour qu'on puisse mieux se rendre compte.

- Les champs position et priority ne sont pas encore utilisés. Pour test, le Chapitre 2 est pas mal car regroupent des CA de type différents (high et lowpriority + `position : side`). Dans le Chapitre 4, il y a un exemple de contenu inline en `position : main`. Mais il faut mettre à jour lankes-html, car ces exemples sont sur lankes.
- Les contenus additionnels ne sont pas "cliquables" et donc pas accessibles. Ex : dossier Ranganathan dans le Chap2 > on a les titres et les infos mais on n'accède pas aux articles. L'url figure bien dans le yaml.
- Affichage des références bibliographiques présentes dans le yaml des CA : apparaissent en note à droite du CA, pas utile. Les enlever du BibTex ou les cacher dans le Front ?
- Redondance en général des infos affichées : Titre + Crédits + Ref biblio

## TO DO
Voici les choses à faire par ordre de priorité :

1. Traiter les références bibliographiques _inline_ avec affichage en hover et un ascenseur (dans le futur).
2. Lorsque la partie "Références" est fermée et que l'on clique sur une référence dans le texte, il ne se passe rien (pas de renvoi et la partie "Références" est non dépliée). Comportement attendu : il faut que lorsqu'on clique sur une référence _inline_, il y ait un renvoi vers la référence complète avec la partie "Référence" dépliée.

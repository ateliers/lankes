# Documentation

## Arborescence

livre
  - chapitre1/
    - chapitre1.md
    - chapitre1.yaml
    - chapitre1.bib
  - chapitre2/
  - introduction/
  - bibliographie/
      - bibliographie.md
      - bibliographie.yaml
      - bibliographie.bib
  - chapitreadd/
    - chapitreadd.md
    - chapitreadd.yaml
    - chapitreadd.bib
  - garde/
    - livre.md
    - livre.yaml
  - media/
    - cvLankes.pdf
  - additionnels.md

---
output/
  - chapitre1.html
  - chapitre2.html
  - introduction.html
  - index.html
---
build.sh

## Typo

Erreurs de typo, coquilles... signalées dans le corps du texte en commentaires
`texte avec coquille corrigée<!-- Typo : il y avait cette coquille ici--> suite du texte`

## Types de source pour les notes de bas de page

À ajouter comme span dans la note : `[^1] [ma note]{.editeur}`

- editeur `{.editeur}`
- annotation (ajout de référence via hypothesis), en précisant le pseudo de l'annotateur et le lien `{.annotation id="pseudo annotation" link="url hypothesis de l'annotation"}`
- auteur (sans ajout de classe)

Les annotations hypothesis de types conversation seront réintégrées comme des annotations hypothesis sous le compte Ateliers/éditeur.

## Types de source pour les références bibliographiques

À ajouter comme marqueur dans les références bibtex :
- editeur
- auteur
- annotation + pseudo de l'annotateur (dans deux marqueurs distincts)


Info complémentaire à ajouter également en marqueur :
- accès libre

## Types de liens

- liens directs pour les cas :
  - organisme
  - personnalité
  - site
  - lieu
- liens transformés en références biblio (à la discrétion de l'éditeur)

## Sémantisation

À mettre à la suite du terme selon le modèle : `[terme]{.étiquette}`
- {.personnalite} (nom propre)
- {.organisme} (Bibliothèque, Université, Laboratoire...)
- {.site} (plateforme, site internet)
- {.lieu} (ville, pays, place)

Ajout d'un id Sens Public (toujours - figurera dans l'index) et autorité quand c'est possible : `[terme]{.étiquette id="autorité"}`
- un id Sens Public - `idsp="Terme tel qu'il s'affichera dans l'index"`
- un id Autorité (quand applicable)
  - Rameau (BnF) `idbnf="https://catalogue.bnf.fr/ark:/12148/cb121792839"`
  - Wikidata `idwiki="http://wikidata.org/345678"`
  - ORCID `idorcid="https://orcid.org/0000-0003-2222-1712"`

Exemples :
[Saddam Hussein]{.personnalite idsp= "Saddam Hussein" idwiki="http://wikidata.org/345678"}
[S. R. Ranganathan]{.personnalite idsp="S. R. Ranganathan" idbnf="https://catalogue.bnf.fr/ark:/12148/cb121792839"}
[[Philadelphia Free Library](https://libwww.freelibrary.org/about/)]{.organisme idsp="Philadelphia Free Library"}
[[Federal Depository Library Program](https://www.fdlp.gov/)]{.site idsp="Federal Depository Library Program"}
[Syracuse]{.lieu idsp="Syracuse" idwiki="https://www.wikidata.org/wiki/Q128069"}
[Isabelle Bastien]{.personnalite idsp="Isabelle Bastien" idorcid="https://orcid.org/0000-0003-2222-1712"}


## Liens internes

- ancre : `[Cushing]{#monancre}`
- appel d'une ancre : `[monlien vers Cushing](#monancre)`

Pour un lien interne dans un même chapitre :
- `[Cushing]{#monancre}` sera appelé par `[monlien vers Cushing](#monancre)`

Pour un lien interne au livre mais d'un chapitre vers un autre :
- `[Cushing]{#monancre}` écrit dans le Chapitre 2 sera appelé dans le Chapitre 6 par `[monlien vers Cushing](chapitre2.html#monancre)`

Pour un lien simple vers un autre chapitre :
- Pour renvoyer au Chapitre 2 depuis le Chapitre 6 le Chapitre 2 sera appelé dans le Chapitre 6 par `[monlien vers Chapitre 2](chapitre2.html)`


## Liens archivés

Les liens archivés (WayBack Machine ou lien permanent Wikipedia) sont indiqués à la suite de chaque url (sauf url non archivables) sous cette forme :

Ex 1 : [[Federal Depository Library Program](https://www.fdlp.gov/){link-archive="https://web.archive.org/web/20190903174843/https://www.fdlp.gov/"}]{.site idsp="Federal Depository Library Program" idwiki="https://www.wikidata.org/wiki/Q5440185"}

Ex 2 : Voir aussi [le rapport annuel de gestion 2015-2016 de la BAnQ](http://www.banq.qc.ca/documents/a_propos_banq/rapports_annuels/BAnQ_RappAnnuelGestion2015-2016_final_br.pdf){link-archive="https://web.archive.org/web/20190830191607/http://www.banq.qc.ca/documents/a_propos_banq/rapports_annuels/BAnQ_RappAnnuelGestion2015-2016_final_br.pdf"}


## Références bibliographiques dans une note

'[^1]: [Voir aussi le texte de Bidouille [-@bidouille] et celui de...]{.editeur}'


## Contenus additionnels & illustrations

On peut continuer à catégoriser les contenus additionnels avec des titres 1. Le build devra supprimer les lignes de titre niveau 1.
On met tous les contenus add. dans un même document : /additionnels.md

Les contenus additionnels peuvent intégrer des médias (pdf, jpeg ou autre).
Pour les indiquer dans le markdown :

[CV de R. David Lankes](media/cvLankes.pdf)

Par défaut les CA apparaissent à côté du chapitre (à la fin - dans une rubrique propre).
Ils peuvent également être *inline* : ils sont alors placés au coeur du texte.


CA "classique"
CA *inline*
Illustrations
Notes

### Illustrations

Les illustrations présentes dans la version originale du livre sont intégrées comme suit :

![Légende[^note]. [Source]{.source}](urlimage.jpg)

[^note]: Bonjour

Le fichier est placé dans le dossier /media


### Types de contenus additionnels

Types de contenus additionnels :
- image
- video
- audio
- article (pour toute référence bibliographique)
- dossier
- pdf
- lien (page web)
- definition


pour chaque type, lister les champs/variables que tu utilises.


### Yaml des contenus additionnels

```yaml
## idDuContenu

---
titre: >-
  'insérer ici le titre'
credits: >-
  'insérer ici les crédits'
keywords: mot cle 1,mot cle 2,mot cle 3...
lang: 'insérer ici la langue du contenu sous le modèle fr, en...'
type: 'insérer ici le type du contenu'
link: 'insérer ici le lien vers le contenu'
link-archive: 'insérer ici le lien archivé vers le contenu - WayBack Machine'
embed: 'insérer ici le code embed'
zotero: @cle_bibtex
date: XXXX
date-publication: 'XXXX-XX-XX' - date de l'ajout du contenu à la publication (par défaut, date de publication de l'ouvrage)
source: préciser l'origine du contenu - 'editeur' / 'auteur' ou 'annotation'
priority: 'lowpriority' ou 'highpriority' (selon si le contenu doit être par défaut ouvert ou fermé)
position: 'main' 'side' ou 'full' (selon si le contenu est dans le corps ou en marge)
---

Descriptif du contenu / Commentaire.
```




### Lien

<div class="lien contenuAdd" id="{{titreNiv2ContenuAdd}}" lang="{{lang}}">
  <h1>{{titre}}</h1>
  <a href="{{link}}" alt="{{title}}">lien</a>
  <a href="{{link-archive}}" alt="{{title}}">lien archivé</a>
  {{contenu}}
  <p class="credits">{{credits}}</p>
  <p class="keywords">{{keywords}}</p>
</div>

### Video

<div class="lien contenuAdd" id="{{titreNiv2ContenuAdd}}" date="{{date}}">
  <h1>{{titre}}</h1>
  <iframe href="{{link}}"/>
  {{contenu}}
  <p class="credits">{{credits}} - {{date}}</p>
  <p class="keywords">{{keywords}}</p>
</div>

## Modèle yaml Chapitre

Exemple :

`---
id: chapitre1
title: 'Le printemps arabe : exigeons l’exceptionnel'
title_f: 'Le printemps arabe&nbsp;: exigeons l’exceptionnel'
subtitle: ''
subtitle_f: ''
lang: fr
year: '2018'
month: '10'
day: '19'
date: 2018/10/19
rights: |-

  Creative Commons Attribution-ShareAlike 4.0 International (CC
  BY-SA 4.0)
url: ''
isbnprint:
isbnepub:
isbnpdf:
isbnnum:
authors:
  - forname: R. David
    surname: Lankes
    orcid: 0000-0001-6975-8045
    viaf: ''
    foaf: ''
    isni: ''
    wikidata: ''
abstract_fr: >-
      Les bibliothèques existent depuis des millénaires, mais sont-elles encore
      d’actualité aujourd'hui&nbsp;? Dans un monde de plus en plus numérique et
      connecté, nos villes, nos collèges et nos écoles doivent-ils encore faire
      de la place aux livres&nbsp;? Et si les bibliothèques ne se résument pas à
      leur collection de livres, quelle est donc leur fonction&nbsp;? Dans son
      ouvrage, Lankes soutient que les communautés, pour prospérer, ont besoin
      de bibliothèques dont les préoccupations dépassent leurs bâtiments et les
      livres qu’ils contiennent. Nous devons donc attendre davantage de la part
      de nos bibliothèques. Elles doivent être des lieux d’apprentissage qui ont
      à cœur les intérêts de leurs communautés sur les enjeux de la protection
      de la vie privée, de la propriété intellectuelle et du développement
      économique. Exigeons de meilleures bibliothèques est un cri de ralliement
      lancé aux communautés pour qu’elles haussent leurs attentes à l’égard des
      bibliothèques.
abstract_en: >-
      Libraries have existed for millennia, but today many question their
      necessity. In an ever more digital and connected world, do we still need
      places of books in our towns, colleges, or schools? If libraries aren’t
      about books, what are they about?

      In Expect More: Demanding Better Libraries For Today’s Complex World,
      Lankes walks you through what to expect out of your library. Lankes argues
      that, to thrive, communities need libraries that go beyond bricks and
      mortar, and beyond books and literature. We need to expect more out of our
      libraries. They should be places of learning and advocates for our
      communities in terms of privacy, intellectual property, and economic
      development.

      Expect More is a rallying call to communities to raise the bar, and their
      expectations, for great libraries.
bibliography: chapitre1.bib
keyword_fr: >-
      bibliothèques, bibliothéconomie, communauté, apprentissage, innovation,
      création de connaissances
keyword_en: 'libraries, librarianship, communty, learning, innovation, knowledge creation'
translator:
  - forname: Jean-Michel
    surname: Lapointe
    orcid: 0000-0001-5123-6011
  - forname: Isabelle
    surname: Bastien
    orcid: ''
  - forname: Lilen
    surname: Colombino
    orcid: ''
  - forname: Marie D.
    surname: Martel
    orcid: 0000-0003-3162-5794
  - forname: Pascale
    surname: Félizat-Chartier
    orcid: ''
  - forname: Adèle
    surname: Flannery
    orcid: ''
  - forname: Catherine
    surname: Forget
    orcid: ''
  - forname: Michael David
    surname: Miller
    orcid: 0000-0002-3548-0249
  - forname: Réjean
    surname: Savard
    orcid: ''
  - forname: Louise
    surname: Struthers
    orcid: ''
  - forname: Ekaterina
    surname: Valkova-Damova
    orcid: ''
pdfurl: ''
coverurl: ''
publisher: Ateliers de Sens public
link-citations: true
nocite: ''
---
`


# FAQ

## Gestion des caractères problématiques

les caractères
- è
- °
- é
- É


# TODO

- Index des entités nommées (voir classes) > clicable
  - une page index de liste d'entités (fait avec basex)
  - entité + liste des occurrences clicables + data à partir de l'autorité
  - dans les chapitres :
    - occurrence est accompagné soit d'un lien + picto qui renvoie vers l'entité dans l'index
- note de type annotation : traitement css + jquery pour linker la source (annotation hs)
  - voir https://stackoverflow.com/questions/16910265/adding-link-a-href-to-an-element-using-css
- note de type editeur : traitement css


- à partir du yaml livre, produire 2 templates :
  - page de garde
  - include aside pour la liseuse
- template chapitre :
  - ajouter le sommaire aside
  - ajouter les infos du chapitre aside
  - ajouter un footer chapitre
- build multi-chapitre

# Structure de la page

- aside : 2 onglets

  1. sommaire livre + infos sur le livre + partager le livre
    - include html produit à partir du yaml du livre (from garde)
  2. sommaire {chapitre + additionnels} +
    - table of content du chapitre
    - infos du chapitre récupéré dans le yaml.


- body

  - barre de navigation/progression avec class = titrechapitre
  - texte
  - contenu additionnel
  - infos de chapitre (metadata)
  - chapeau/note éditeur


footer :
  - infos sur le livre avec image du livre,
  - isbn,
  - lien à la page de présentation du livre,
  - date version,
  - référence à citer,
  - licence

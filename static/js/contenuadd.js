document.addEventListener('DOMContentLoaded', () => {
  /* Expand contenuadd with lowpriority. */
  const contenuadds = document.querySelectorAll('.contenuadd.lowpriority > h3')
  Array.from(contenuadds).forEach(contenuadd => {
    contenuadd.addEventListener('click', e => {
      const section = e.target.closest('section')
      section.classList.toggle('expanded')
      e.preventDefault()
    })
  })
})

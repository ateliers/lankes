document.addEventListener('DOMContentLoaded', () => {
  /* Navigation with the link from the header. */
  const mainNav = document.querySelector('header.main nav[aria-role="main"]')
  const titleLink = document.querySelector('header.main h2 a')
  window.addEventListener('click', e => {
    if (!mainNav.contains(event.target) && event.target !== titleLink) {
      mainNav.classList.remove('opened')
    }
  })
  titleLink.addEventListener('click', e => {
    e.preventDefault()
    mainNav.classList.toggle('opened')
  })
})

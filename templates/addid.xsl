<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

  <xsl:output omit-xml-declaration="yes"/>
<xsl:template match="*">
      <xsl:copy>
        <xsl:attribute name="id"><xsl:value-of select="generate-id()"/></xsl:attribute>
        <xsl:for-each select="@*">
          <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
        </xsl:for-each>
        <xsl:apply-templates/>
      </xsl:copy>
    </xsl:template>
</xsl:stylesheet>

xquery version "3.1";

declare namespace functx = "http://www.functx.com";
declare namespace html = "http://www.w3.org/1999/xhtml";

(: ##### ouvre la base XML ####:)
let $chapitres := db:open("lankes")/html/body/article

(: ##### ouvre un document HTML #### :)
(: quand le script exec sera en mesure de traiter tous les chapitres à la fois,
il faudra utiliser une "collection", voir https://www.abbeyworkshop.com/howto/xml/xql-saxon-basics/ :)

(: let $chapitres := fn:doc("/home/nicolas/gitlab/process-local-lankes/output/chapitre7.html")/html:html/html:body/html:article :)


(: let $chapitres := db:create("lankesnew") :)

(: for $chapitres in collection("/home/nicolas/gitlab/process-local-lankes/output") :)

(: return $chapitres :)


let $mapOrganisme := map:merge(
  for $occurence in $chapitres//*[@class='organisme']
    let $chapitre := fn:substring-after(fn:substring-after(fn:base-uri($occurence),'/'),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $sortedorganisme := (
  for $cle in map:keys($mapOrganisme)
    order by $cle 
    return $cle
  )

let $organismes := for $cle in $sortedorganisme
  let $terme :=  <p id="{$cle}">{$cle} - {
        for $value in $mapOrganisme($cle)
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
   return $terme


let $mapLieu := map:merge(
  for $occurence in $chapitres//*[@class='lieu']
    let $chapitre := fn:substring-after(fn:substring-after(fn:base-uri($occurence),'/'),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $sortedlieu := (
  for $cle in map:keys($mapLieu)
    order by $cle 
    return $cle
  )

let $lieux := for $cle in $sortedlieu
  let $terme :=  <p id="{$cle}">{$cle} - {
        for $value in $mapLieu($cle)
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
   return $terme

let $mapPersonnalite := map:merge(
  for $occurence in $chapitres//*[@class='personnalite']
    let $chapitre := fn:substring-after(fn:substring-after(fn:base-uri($occurence),'/'),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $sortedpersonnalite := (
  for $cle in map:keys($mapPersonnalite)
    order by $cle 
    return $cle
  )

let $personnalites := for $cle in $sortedpersonnalite
  let $terme :=  <p id="{$cle}">{$cle} - {
        for $value in $mapPersonnalite($cle)
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
   return $terme

let $mapSite := map:merge(
  for $occurence in $chapitres//*[@class='site']
    let $chapitre := fn:substring-after(fn:substring-after(fn:base-uri($occurence),'/'),'/')
    let $id := $occurence/@id
    return map {$occurence/@data-idsp : concat($chapitre,'#',$id)},
    map { 'duplicates': 'combine' }
  )

let $sortedsite := (
  for $cle in map:keys($mapSite)
    order by $cle 
    return $cle
  )

let $sites := for $cle in $sortedsite
  let $terme :=  <p id="{$cle}">{$cle} - {
        for $value in $mapSite($cle)
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
   return $terme


(: let $allOrganisme :=
  <div class="organisme"><h2>Organismes</h2>{
    map:for-each($mapOrganisme, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
      return $occurence
    }
    )
  }
  </div>

let $allLieu :=
  <div class="lieu"><h2>Lieux</h2>{
    map:for-each($mapLieu, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
      return $occurence
    }
    )
  }
  </div>

let $allPersonnalite :=
  <div class="personnalite"><h2>Personnalités</h2>{
    map:for-each($mapPersonnalite, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
       }</p>
      return $occurence
    }
    )
  }
  </div>

let $allSite :=
  <div class="site"><h2>Sites</h2>{
    map:for-each($mapSite, function($key, $values) {
      let $occurence :=  <p id="{$key}">{$key} - {
        for $value in $values
          count $id
          let $link := <a href="{$value}" class="occurrence">{$id}</a>
          return $link
      }</p>
      return $occurence
    }
    )
  }
  </div> :)


let $index :=
	<div class="index">
    <div class="organisme">
      <h2>Organismes</h2>
      {$organismes}
    </div>
    <div class="personnalite">
      <h2>Personnalités</h2>
      {$personnalites}
    </div> 
    <div class="lieu">
      <h2>Lieux</h2>
      {$lieux}
    </div> 
    <div class="site">
      <h2>Site</h2>
      {$sites}
    </div>  
</div>    
    
(: return file:write('/home/nicolas/gitlab/process-local-lankes/index.html', $index, map { 'method' : 'xml', 'indent' : 'yes', 'omit-xml-declaration' : 'no'}) :)
return $index


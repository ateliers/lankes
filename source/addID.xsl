<xsl:stylesheet version="2.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:dc="http://purl.org/dc/terms/"
                xmlns:foaf="http://xmlns.com/foaf/0.1/">

  <xsl:output doctype-system="about:legacy-compat" method="xhtml"
     omit-xml-declaration="yes" />

  <!-- By default, copy elements and attributes unchanged -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- recopier les namespace foaf et dc -->
      <!-- xmlns:dc="http://purl.org/dc/terms/" -->
      <!-- xmlns:foaf="http://xmlns.com/foaf/0.1/" -->

  <xsl:template match="*:html">
    <!-- <html xmlns="http://www.w3.org/1999/xhtml"
          xmlns:dc="http://purl.org/dc/terms/"
          xmlns:foaf="http://xmlns.com/foaf/0.1/"
          xml:lang="fr">

      <xsl:apply-templates select="node()|@*"/>

    </html> -->

    <xsl:copy>
      <xsl:attribute name="dc" namespace="http://purl.org/dc/terms/">http://purl.org/dc/terms/</xsl:attribute>
      <xsl:attribute name="foaf" namespace="http://xmlns.com/foaf/0.1/"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>


  <!-- modifier les span d'entité nommée (contenant un attribut @data-idsp) -->
  <xsl:template match="*:span[@data-idsp]">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="id" select="generate-id()"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>

  </xsl:template>

  <!-- placer les références dans une balise detail/summary -->
  <xsl:template match="*:section[@class = 'level2 unnumbered']">
      <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <details>
          <xsl:attribute name="class" select="*:div/@class/data()"/>
          <xsl:attribute name="id" select="*:div/@id/data()"/>
          <summary>
            <xsl:value-of select='./*:h2/data()'/>
    	  </summary>
          <xsl:apply-templates select="node()"/>
        </details>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="*:h2[parent::*:section[@class = 'level2 unnumbered']]">
  </xsl:template>


</xsl:stylesheet>
